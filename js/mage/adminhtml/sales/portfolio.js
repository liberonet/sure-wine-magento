
function showWindow(url,title){
var win = new Window({ title: title, url:url, zIndex:3000, destroyOnClose: true, recenterAuto:true, resizable: false, width:650, height:300, minimizable: false, maximizable: false, draggable: true});
win.showCenter(true);
window.win = win;
}