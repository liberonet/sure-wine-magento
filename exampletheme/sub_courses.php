<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/title_courses.png" />
                    
                    <p><img style="margin-right:25px;" src="images/courses_img01.jpg" align="left"  />To add your wine course to this list please contact <a href="mailto:info@sureholdings.com">info@sureholdings.com </a></p>
                    <p><strong><red>Wine Courses Companies</red></strong></p>
                    <p>Wine &amp; Spirit Education Trust<br />
                    <a href="http://www.wset.co.uk" target="_blank">www.wset.co.uk</a>, <a href="mailto:school@wset.co.uk">school@wset.co.uk</a></p>
                    <p>Malmö Wine Academy<br />
                      <a href="http://www.malmo-wine-academy.com" target="_blank">www.malmo-wine-academy.com</a>, <a href="mailto:info@malmo-wine-academy.com">info@malmo-wine-academy.com</a></p>
                </td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>