<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/title_contact.png" />
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td width="108" rowspan="3" align="left" valign="top"><p><img src="images/contact_img01.jpg" hspace="15" /></p></td>
                        <td colspan="5" align="left" valign="top"><p><strong>
                          <red>Sure Holdings Ltd</red>
                        </strong></p>
                        <p>General Enquiries<br />
                          <a href="mailto:Customerservices@sureholdings.com">Customerservices@sureholdings.com</a></p>
                        <p>Accounts and Billing Enquiries<br />
                          <a href="mailto:Accounts@sureholdings.com">Accounts@sureholdings.com</a></p>
                        <p>For more Information on our products or services<br />
                          <a href="mailto:info@sureholdings.com">info@sureholdings.com</a></p></td>
                      </tr>
                      <tr>
                        <td align="left" valign="top">
                        	<p class="small"><red>Malaysia</red><br />
                        	  <black><em>Correspondence Address:</em></black>
                        	  <br />
                        	  Suite# 105 <br />
                        	  G-0-10, Plaza Damas 60,<br />
                        	  Jln Sri Hartamas 1, Sri Hartamas,<br />
                        	  50480, Kuala Lumpur, Malaysia. </p>
                       	<p class="small">
                       	  <black><em>
                       	    <red>CS </red><br />
                   	      CS Trust's Address:</em></black>
                       	  <br />
Unit B, Lot 49, 1st Floor,<br />
Block F, Lazenda Warehouse 3,<br />
Jalan Ranca-Ranca,<br />
87000 F.T. Labuan,<br />
Malaysia.                        </p></td>
                        <td width="20" rowspan="2" align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="top"><p class="small"><red>
                          Singapore</red><br />
                          Fine Wine Management Pte Ltd<br />
                          10 Anson Road<br />
                          #26-04 International Plaza<br />
                          Singapore 079903<br />
                          Tel: +65 92378786</p></td>
                        <td width="20" rowspan="2" align="left" valign="top">&nbsp;</td>
                        <td align="left" valign="top"><p class="small">
                          <red> United Kingdom</red><br />
                          82 King Street<br />
                          Manchester<br />
                          M2 4WQ<br />
                          Tel: +44 07921133478</p></td>
                      </tr>
                    </table>
                    
                </td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>