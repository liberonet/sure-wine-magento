<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/title_investment.png" />
                    <p>There are two major reasons to invest in wine:</p>
               	  <ul>
                            <li>
                            Firstly as an investment in future drinking - buying young wines at the initial release price which, when mature, would be considerably more expensive to buy.
                            </li>
                            <li>As a strictly financial investment - buying wines with the sole intention of reselling later for a profit.</li>
	                   </ul>
                  <p>For many people who keep a cellar, both reasons play a part.</p>
                   <p>The global demand for fine wine, which is produced in very small quantities, has increased enormously over the last two decades. Wine can, and often has, outperformed the FTSE 100 and the Dow Jones, offering significant returns without the volatility of the stock market.</p>
                   <p><img src="images/investment_img01.jpg" align="right" /><strong><red>Click on the titles below to read more:</red></strong></p>
                   		<ul>
                            <li><a href="#">Wine Recommendations</a></li>
                            <li><a href="#">Wine Investment Essentials</a></li>
                            <li><a href="#">Counterfeits and Other Problems</a></li>
                   			<li><a href="#">Other Common Wine Faults</a></li>
                   		</ul>
                   </td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>