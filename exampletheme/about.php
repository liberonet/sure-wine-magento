<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/title_about.png" />
                	<p><strong>SURE HOLDINGS</strong> Property and Wine Club firmly believes that it is essential that our clients not only invest in quality property that can be turned into amazing profit - but also learn and understand everything that there is to know about property, purchasing off-plan, the selling process, rental process so on and so forth as we wish to offer a modern approach to property investment. </p>
                	<p>Property and Fine Wine investment will always sustain an element of risk. Therefore, it is our job to minimize this risk factor for you by carrying out thorough research into every opportunity we show to you. The largest asset and biggest step many people ever make will be acquiring property, at the same time it is also one of the most exciting and rewarding steps. This will also be, more often than not, the asset that will achieve the highest growth in your lifetime. </p>
               	<p>It is the intention of the company and Directors to operate in an honest and fully transparent manner at all times and with all our dealings with members. <strong>SURE HOLDINGS</strong> wishes to function in this manner, as we would like our clients to be positive and fully comfortable with our services to ensure we operate in a professional and ethical manner at all times. </p></td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>