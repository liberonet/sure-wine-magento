<table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="left" valign="top" class="rightPan">
    	<?php
			if ($thisPage == 'sub_investment.php')
				echo 'Making wine investment';
	
			else
				echo '<a href="sub_investment.php">Making wine investment</a>';
		?>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" class="rightPan">
    	<?php
			if ($thisPage == 'sub_investBenefits.php')
				echo 'Wine investment benefits';
	
			else
				echo '<a href="sub_investBenefits.php">Wine investment benefits</a>';
		?>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" class="rightPan">
    	<?php
			if ($thisPage == 'sub_how2.php')
				echo 'How to invest';
	
			else
				echo '<a href="sub_how2.php">How to invest</a>';
		?>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" class="rightPan">
    	<?php
			if ($thisPage == 'sub_trackRecord.php')
				echo 'Our track record';
	
			else
				echo '<a href="sub_trackRecord.php">Our track record</a>';
		?>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" class="rightPan">
    	<?php
			if ($thisPage == 'sub_topWines.php')
				echo 'Top wines in media';
	
			else
				echo '<a href="sub_topWines.php">Top wines in media</a>';
		?>
    </td>
  </tr>
  <tr>
    <td align="left" valign="top" class="rightPan">
    	<?php
			if ($thisPage == 'sub_courses.php')
				echo 'Wine courses';
	
			else
				echo '<a href="sub_courses.php">Wine courses</a></td>';
		?>
  </tr>
</table>

<div class="btnRegister">
    <a href="register.php"><img src="images/spacer.gif" width="269" height="87" /></a>
</div>