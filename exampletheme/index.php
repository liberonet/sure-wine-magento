<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/index_img01.jpg" />
                	<p>Fine Wine investment is becoming an increasingly attractive option to innovative investors who wish to gain more out of their investments. We concentrate on the best Chateaux from Bordeaux, Burgundy and the Rhone Valley.  We stick to what we know best. We purchase stock in bulk to offer some of the best prices out in the market place today.<br />
                	  <br />
                	  Our strategy for Vintage Fine Wine trading is focused on Blue Chip wines from specific regions with recognised classifications that have been around for hundreds of years and are vigorously upheld today and controlled by the Institut D’Appellations d’Origine and upheld by European Committee directives. It is true that the wines we offer are considered the most expensive wines in the market. However, these wines offer stable returns with lower risk and the quality of the wines has been proven they will last for more than 50 years. A fine wine portfolio can be turned for years without additional investment. <br />
              	  </p>
                </td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>