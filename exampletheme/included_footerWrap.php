<tr>
    <td id="footerWrap">
        <div id="quickLinks">
            <table width="963" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td height="17" colspan="4" align="left" valign="top" bgcolor="#e5e5e5" class="h1">QUICK LINKS</td>
              </tr>
              <tr>
                <td height="25" colspan="4"></td>
              </tr>    
              <tr>
                <td width="25%" align="left" valign="top" class="category">
                    <div class="h2">INFORMATION</div>
                    <a href="index.php">Home</a><br />
                    <a href="wine.php">Wines</a><br />
                    <a href="about.php">About</a>
                </td>
                <td width="25%" align="left" valign="top" class="category borderLeft">
                    <div class="h2">ABOUT WINE</div>
                    <a href="sub_investment.php">Making wine investment</a><br />
                    <a href="sub_investBenefits.php">Wine investment benefits</a><br />
                    <a href="sub_how2.php">How to invest</a><br />
                    <a href="sub_trackRecord.php">Our track record</a><br />
                    <a href="sub_topWines.php">Top wines in media</a><br />
                    <a href="sub_courses.php">Wine courses</a><br />
                </td>
                <td width="25%" align="left" valign="top" class="category borderLeft">
                  <div class="h2"><a href="portfolio.php">MY PORTFOLIO</a></div>
                  <a href="#">My Orders</a><br />
                  <a href="#">My Addresses</a><br />
                  <a href="#">My Profile</a><br />
                  <a href="#">My Credit Slips</a><br />
                  <a href="#">My Vouchers</a></td>
                <td width="25%" align="left" valign="top" class="category borderLeft">
                  <div class="h2"><a href="contact.php">CONTACT US</a></div></td>
              </tr>
            </table>
    
      </div>    
      <span id="copyright">© 2000 - Sure Holdings. All rights reserved.</span>    
    </td>
</tr>