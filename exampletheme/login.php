<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="3" align="left" valign="top">
               	  <p><img src="images/title_login.png" /></p>
                  <p><img src="images/arrow_right.png" vspace="3" align="left" />Passwords are required to be a minimum of 6 characters in length. </p>
                </td>
              </tr>
              <tr>
                <td width="480" align="left" valign="top">
                    <form>
                    <table width="480" border="0" cellspacing="0" cellpadding="0" bgcolor="#efeeef">
                      <tr>
                        <td height="57" colspan="2" align="left" valign="top"><img src="images/form_bg1.png" /></td>
                      </tr>
                      <tr>
                        <td width="88">&nbsp;</td>
                        <td width="392" height="32" align="left" valign="middle">Email</td>
                      </tr>
                       <tr>
                        <td width="88">&nbsp;</td>
                        <td width="392" height="32" align="left" valign="middle">
                         <input name="" type="text" class="textField" id="" /></td>
                      </tr>
                      <tr>
                        <td width="88">&nbsp;</td>
                        <td width="392" height="32" align="left" valign="middle">Password</td>
                      </tr>
                       <tr>
                        <td width="88">&nbsp;</td>
                        <td width="392" height="32" align="left" valign="middle">
                         <input name="" type="password" class="textField" id="" /></td>
                      </tr>
                      <tr>
                        <td width="88">&nbsp;</td>
                        <td width="392" height="32" align="left" valign="middle">
                        	<input type="checkbox" /> Remember me?
                        </td>
                      </tr>
                       <tr>
                        <td width="88">&nbsp;</td>
                        <td width="392" height="70" align="left" valign="bottom">
                        	<div class="btnLogin">
                            	<input name="submit" type="image" src="images/spacer.gif" alt="Submit Form" width="178" height="50" /></a>
                            </div>
                        </td>
                      </tr>
                      <tr>
                        <td width="88">&nbsp;</td>
                        <td width="392" height="32" align="left" valign="middle"><p style="font-weight:normal;"><a href="register.php">Register</a> if you don't have an account.</p></td>
                      </tr>
                      <tr>
                        <td height="43" colspan="2" align="left" valign="bottom"><img src="images/form_bg2.png" /></td>
                      </tr>
                    </table>
                  </form>
                    <table width="480%" border="0" cellspacing="0" cellpadding="0" bgcolor="#efeeef">
                      <tr> </tr>
                    </table>
                </td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="458" align="left" valign="top"><img src="images/login_img01.jpg" border="0" usemap="#Map" /></td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>

<map name="Map" id="Map">
  <area shape="rect" coords="139,117,208,141" href="#" />
</map>
</body>
</html>