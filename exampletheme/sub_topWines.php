<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/title_topwinesMedia.png" />
                  <p><strong>
                      <red>Click on the titles to read the aticles:</red>
                    </strong></p>
                   		<ul><li> <a href="#">Dan Berger's Wine Experience</a></li>
                   		  <li> <a href="#">Champagne is the pick of the crop in the wine market</a></li>
                   		  <li><a href="#">Investing in Wine: Returning 522%, Some Wines Are Outpacing Stocks</a></li>
                   		  <li><a href="#">Liquorland Top 100 2005 Competitions</a></li>
                   		  <li><a href="#">Incentive to cut wine overproduction offered</a></li>
               		    </ul>
           		   <p><img src="images/topWines_img01.jpg" width="647" height="299" /></p></td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>