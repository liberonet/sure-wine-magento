<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/title_investBenefits.png" />
               	  <p><strong>
               	  <red>Capital Gains Tax (CGT)</red></strong> is a tax on profit made when you sell assets or investments. These can be anything from holiday homes to works of art, shares or the goodwill of a business.</p>
                  <p>If you sell or transfer these assets to someone else for more than you paid for them, you may have made a capital gain. If you give assets away to anyone close to you (apart from your spouse) when they are worth more than you paid for them, for tax purposes you may have made a capital gain.</p>
                  <p><strong><red>Exemptions</red></strong><br />
                  Wine is not subject to UK Capital Gains Tax as the authorities look upon it as a wasting asset, so any profit derived belongs to the client.</p>
                  <p>Key points for tax free wine investment:</p>
                  <ul>
                  	<li>avoid trading status</li>
                    <li>use the wasting asset or chattels exemptions</li>
                    <li>keep records to justify your self assessment and make full disclosure on your tax return</li>
                  </ul>
                  <p>Fine wine has shown over time to generate in excess of 11% per annum on average. Prices can and do vary over time, but if one purchases very wisely, the risks can be minimised. Great wine is always in demand. Wine appreciates in value because it is constantly being consumed.</p>
                  <p>Experts predict that due to a strictly limited supply on one side, and increased international demand on the other, there is no foreseeable reason why this growth should not continue and develop further.</p>
                  <p>In addition to being one of the most profitable and tax-efficient investments available, investment in fine Claret tends to be unaffected by unpredictable events or fluctuating world economies that can influence our own Stock Market.</p>
                  <p>Fine Wine has many advantages over other forms of investments, such as unit trusts, life assurance, investment bonds and equities. Fine Wine benefits from being stable, tax-free, easily realisable, consumable, low risk and portable. Above all else, due to decreasing availability, there is increasing demand. Fine Wine has consistently outperformed all other forms of recognised investment. It has remained the steadiest form of investment, as it is generally unaffected by recession, interest rate changes and stock market fluctuations.</p>
                  <p>In comparison to other alternative investments, Fine Wine has clear advantages over investments such as art or property. Fine Wine is easily stored and generally increases in quality the longer it is left. Investing in either art or property requires expensive maintenance and care, while Fine Wine needs no such attention.</p>
                  <p>Fine Wine investment is becoming an increasingly attractive option to innovative investors who wish to gain more out of their investments.</p></td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>