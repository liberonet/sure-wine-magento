<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	
    <?php include 'included_head.php'; ?>
</head>

<body>
	<table width="970" border="0" align="center" cellpadding="0" cellspacing="0">
      <?php include 'included_headerWrap.php'; ?>
      <tr>
        <td id="middleWrap">
        	<table width="100%" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td width="650" align="left" valign="top">
                	<img src="images/title_how2.png" />
               	  <p><strong><red>The Six Steps to building a portfolio with Sure Holdings</red></strong></p>
               	  <p>&nbsp;</p>
               	  <p><strong>
               	    <red>1. Building a Portfolio</red>
               	  </strong><br />
                  The traditional method of investing into wine is per case basis. Depending on what wines we have available at the time, your broker will contact you to inform you of the wines we currently have  top on our list for investment. If these wines are suitable for your portfolio then your broker will be in contact and advise you accordingly.</p>
                  <p><strong><red><br />
                    2. Wine Futures</red></strong><br />
                  Wine Futures (also known as &quot;En Primeur&quot;) refers to buying wine after it is made, but before it is bottled. Cask samples of wines are made available for tasting to wine journalists and the large wholesale buyers in the spring following the vintage. The brokers and merchants sell on the wine to their customers. The wine is generally bottled and shipped around two years later. In good vintages, wine futures can offer an investor the greatest return; the initial release prices are usually the lowest at which the wines will ever be sold.</p>
                  <p>Wine is a transferable asset. Fine wine has benefited from a dramatic increase in popularity and there is an established and thriving fine wine auction market. There is no limit to the amount you can invest in wine and being a 'perishable' item, wine is not subject to capital gains tax. If you don't realise a return on your wine investment, you can always enjoy drinking it!</p>
                  <p><strong>
                  <red><br />
                  3. Benefits</red></strong><br />
                    Assuming we have wines readily available - either in Bordeaux or from UK-held bonded stocks - which fit your investment requirements, and after acceptance of our quotation, an invoice in British Pound per case (12 bottles) will be sent to you for immediate payment. Alternatively you can settle with credit card. Credit card charges come with a 5% handling charge. The wines are sold &quot;lying in bond UK&quot;, although as stated above, they may be shipped over from stocks held in Bordeaux - a process which takes 2- 3 weeks.</p>
                  <p><strong>
                  <red><br />
                  4. Storage</red></strong><br />
                    Unless otherwise instructed, for storage, the Bonded Warehouse of our choice is London City Bond (LCB). The wine will be registered in your name, giving you complete independence and control over your stock. LCB is one of the UK's premier bonded wine, warehousing and distribution companies handling over four million cases of wine a year for both private and trade customers.</p>
                  <p>The bonded warehouse has the capacity of its unique cellars to store over 800,000 cases in ideal conditions, 90 feet underground providing:</p>
                  <ul>
                    <li>Constant computer controlled temperature</li>
                    <li>Absence of ultraviolet light</li>
                    <li>Full humidity control</li>
                  </ul>
                  <p>Blending the best of traditional storage and delivery skills with the most advanced technology, you can be Sure that your wine is stored correctly.</p>
                  <p>Rent is currently charged at British Pound 10.50 per case per year or part thereof, and will be invoiced by Sure Holdings directly.</p>
                  <p>We strongly recommend clients store their wine In Bond, as storage conditions are ideal for long term cellaring and this is taken into account when the time comes to re-sell. Also, storing In Bond saves unnecessary capital outlay on VAT and Duty.</p>
                  <p><strong><red><br />
                    5. Insurance</red></strong><br />
                  If the wine is stored at Our bonded warehouses, any investment wine you undertake with Sure Holdings is covered with &quot;All Risk Insurance&quot; at no extra cost which covers your stock in the event of breakage or loss to full replacement value.</p>
                  <p><strong><red><br />
                    6. Tax Free Capital Growth</red></strong><br />
                    The careful employment of capital in Fine Claret has proved to be a lucrative and safe investment during the last thirty years or so, often out-performing alternative types of investment.</p>
                  <p>Records of U.K. auction prices published every month between August 1978 and July 2000 by Decanter Magazine show that a selection of leading Bordeaux Classified Growths have performed consistently well. It is particularly significant, that at least half the twenty vintages monitored are not what we would consider to be &quot;investment vintages&quot;, which illustrates how exciting the growth factor would have been if only top years had been included in this survey.</p>
                  <p>From a starting point of 100 in August 1978, the Decanter &quot;Bordeaux Index&quot; had risen to 1155.05 in July 2000. <strong>A tax free increase of 1055% for that period. This is equivalent to an average annual growth of 11.75% compounded.</strong></p>
                  <p><strong>
                  <red><br />
                  7. Selling Your Wine</red></strong><br />
                  With effect from Friday 9th May 2008, the valuation on all wines in our website and on clients' portfolio's will not deduct 10%. Previously 10% was deducted from the Bid price on all our wine valuations due to the costs of selling wines to any Broker, Merchant or Auction House in Europe. In future when selling your wines instead the 10% will be taken from the price when selling. Please note that 10% is the margin that any reputable Broker, Merchant or Auction House would take when buying wines. The Bid and Ask price will still continue as this is our guidelines to market prices in the wine index.</p></td>
                <td align="left" valign="top">&nbsp;</td>
                <td width="269" align="left" valign="top">
                	<?php include 'included_rightPannel.php'; ?>                
                </td>
              </tr>
            </table>
        </td>
      </tr>
      <?php include 'included_footerWrap.php'; ?>
    </table>
</body>
</html>