<?php
class LiberoNet_Credit_Model_Resource_Credit_Collection extends Mage_Sales_Model_Resource_Order_Collection
 {
     public function _construct()
     {
         parent::_construct();
         $this->_init('credit/credit');
     }
}