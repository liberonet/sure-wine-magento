<?php
   class LiberoNet_Credit_Helper_Data extends Mage_Core_Helper_Abstract
   {

//	check if a product is Storage Fee(SKU 358-360)
    public function isStorage($id){
    	return in_array($id, array('374','375','376','379','380','381','382','383','384','385','386','387'));
    }

//	check if a product is Wine according to id
    public function isWine($id){
    	return !in_array($id, array('358','359','360','361','362','363','364','365','366','367','368','369','370','371','372','373','374','375','376','377','378','379','380','381','382','383','384','385','386','387','399','400','401'));
    }
   }