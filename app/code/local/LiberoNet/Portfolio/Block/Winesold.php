<?php
class LiberoNet_Portfolio_Block_Winesold extends Mage_Core_Block_Template
{
     public function __construct()
    {
        parent::__construct();
        $this->setTemplate("portfolio/winesold.phtml");
        $portfolios_sold = Mage::getResourceModel("portfolio/portfolio_collection")
                ->addFieldToSelect('*')
                ->addFieldToFilter('status','sold')
                ->addFieldToFilter('customer_id',Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->setOrder('created_at', 'desc');
        $portfolios_in_sale = Mage::getResourceModel("portfolio/portfolio_collection")
                ->addFieldToSelect('*')
                ->addFieldToFilter('status','pending')
                ->addFieldToFilter('customer_id',Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->setOrder('created_at', 'desc');
        $this->setPortsSold($portfolios_sold);
        $this->setPortsInSale($portfolios_in_sale);
    }

    protected function _prepareLayout()
    {
        $this->getPortsSold()->load();
        $this->getPortsInSale()->load();
        return $this;
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

}