<?php
class LiberoNet_Portfolio_Block_Add extends Mage_Core_Block_Template
{
     public function __construct()
    {
        parent::__construct();
        $this->setTemplate("portfolio/add.phtml");
    }

    protected function _prepareLayout()
    {
        return $this;
    }

}