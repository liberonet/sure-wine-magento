<?php
class LiberoNet_Portfolio_Block_Edit extends Mage_Core_Block_Template
{
     public function __construct()
    {
        parent::__construct();
        $this->setTemplate("portfolio/edit.phtml");
    }

    protected function _prepareLayout()
    {
        return $this;
    }

    public function getActions(){
    	$action_sell = '<input type="radio" id="action_sell" name="action" value="sell">Sell';
    	$action_edit_sell = '<input type="radio" id="action_sell" name="action" value="sell">Edit Sell';
    	$action_complete_sell = '<input type="radio" id="action_complete_sell" name="action" value="complete_sell">Complete Sell';
    	$action_change_qty = '<input type="radio" id="action_change_qty" name="action" value="delete">Change Quantity';
    	$action_shipping_from = '<input type="radio" id="action_shipping_from" name="action" value="shipping_from">Edit Shipping From';
        $action_cancel_selling = '<input type="radio" id="action_cancel_selling" name="action" value="cancel_selling">Cancel Selling';
        $action_change_stage = '<input type="radio" id="action_change_stage" name="action" value="change_stage">Change Stage';
    	$ret = '';
    	if($this->getStatus() == "portfolio"){
    		$ret .= $action_sell.$action_complete_sell.$action_change_qty.$action_change_stage;
    	}else if($this->getStatus() == "pending"){
    		$ret .= $action_edit_sell.$action_complete_sell.$action_change_qty.$action_cancel_selling.$action_change_stage;
    	}else if($this->getStatus() == "sold"){
    		$ret .= $action_change_qty.$action_shipping_from.$action_change_stage;
    	}
    	return $ret.'<br>';
    }

}