<?php
class LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tabs extends Mage_Adminhtml_Block_Customer_Edit_Tabs
{
    protected function _beforeToHtml()
    {
        $this->addTab('portfolio', array(
                'label'     => Mage::helper('catalog')->__('Portfolio'),
                'content'   => $this->getLayout()->createBlock('LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Portfolio')->toHtml(),
            ));

        $this->addTab('wine_sold', array(
                'label'     => Mage::helper('catalog')->__('Wine Sold'),
                'content'   => $this->getLayout()->createBlock('LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Sold')->toHtml(),
            ));
        $this->addTab('wine_selling', array(
                'label'     => Mage::helper('catalog')->__('Wine Selling'),
                'content'   => $this->getLayout()->createBlock('LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Grid')->toHtml(),
            ));
            return parent::_beforeToHtml();
    }
 }