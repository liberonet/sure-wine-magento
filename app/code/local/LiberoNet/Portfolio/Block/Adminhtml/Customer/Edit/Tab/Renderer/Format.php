<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml alert queue grid block action item renderer
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Format
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Array to store all options data
     *
     * @var array
     */

    public function render(Varien_Object $row)
    {   
        if(!$row->getData('case_size'))return null;
        $case_size = 0;
        switch (intval($row->getData('case_size'))) {
        case 3:
            $case_size = 12;
            break;
        case 4:
            $case_size = 6;
            break;
        case 5:
            $case_size = 1;
            break;
        case 6:
            $case_size = 3;
            break;
        default:
            # code...
            break;
    }
        return $case_size.'x'.$row->getData('bottle_size');
    }

    
}
