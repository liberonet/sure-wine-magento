<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Adminhtml
 * @copyright   Copyright (c) 2013 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Adminhtml alert queue grid block action item renderer
 *
 * @category   Mage
 * @package    Mage_Adminhtml
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_PriceEuro
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * Array to store all options data
     *
     * @var array
     */

    public function render(Varien_Object $row)
    {   

        $_fromCurr = "GBP";
        $_toCurr = "EUR";
        $currency = new Zend_Currency('en_US'); 
        $amount = $row->getData($this->getColumn()->getIndex());
        return $_price = $currency->toCurrency(round( Mage::helper('directory')->currencyConvert( $amount, $_fromCurr, $_toCurr ), 2 ),array('currency'=>'EUR'));
        // return '<a href="'.$this->getUrl('adminhtml/sales_order/view',array('order_id'=>$row->getData('order_id'))).'">'.$row->getData($this->getColumn()->getIndex()).'</a>';
    }

    
    public function renderExport(Varien_Object $row)
    {   
        $_fromCurr = "GBP";
        $_toCurr = "EUR";
        $currency = new Zend_Currency('en_US'); 
        $amount = $row->getData($this->getColumn()->getIndex());
        if(!$amount)return null;
        return $_price = round( Mage::helper('directory')->currencyConvert( $amount, $_fromCurr, $_toCurr ), 2 );
    }

    
}
