<?php
class LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Sold extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
    {
        parent::__construct();
        $this->setId('portfolio_sold_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
      $collection = Mage::getResourceModel("portfolio/portfolio_collection")
                    ->addFieldToSelect('*')
                    ->addFieldToSelect('sold_at')
                    ->addFieldToSelect('receiving_price')
                    ->addFieldToSelect('id_libero_portfolio','id_main')
                    ->addFieldToSelect('status','status_main')
                    ->addFieldToSelect('qty','qty_main')
                    ->addFieldToFilter('customer_id',Mage::registry('current_customer')->getId())
                    ->addFieldToFilter('main_table.status','sold')
                    ->join(array('item'=>'sales/order_item'),'main_table.item_id=item.item_id',array('order_id','base_price'))
                    ->join(array('product'=>'catalog/product'),'main_table.product_id=product.entity_id')
                    ->join(array('order'=>'sales/order'),'item.order_id=order.entity_id',array('base_currency_code','created_at','increment_id'))
                    ->join(array('order_grid'=>'sales/order_grid'),'item.order_id=order_grid.entity_id','shipping_name')
                    ;
        // $collection->getSelect()
        //     ->joinLeft(array('rma'=>'rma_products'),'main_table.item_id=rma.rp_orderitem_id and rma.rp_object_type="creditmemo"','rp_object_id')
        //     ->joinLeft(array('creditmemo'=>''))
                    //->setOrder('order.created_at', 'desc')
                    ;
                    // var_dump($collection->getSelect()->__toString());
                    $prodNameAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'name')
                                    ->getAttributeId();
                    
                    $collection = Mage::helper('portfolio')->getPricesCollection($collection,'product');

        $collection->getSelect()
                    ->joinLeft(array('credit'=>'libero_flat_credit'),'main_table.credit_id=credit.entity_id','shipping_from')
                    ->joinLeft(
                    array('cpev' => 'catalog_product_entity_varchar'), 
                    'cpev.entity_id=product.entity_id AND cpev.attribute_id='.$prodNameAttrId.'', 
                    array('name' => 'value')
                    );
                    $this->setCollection($collection);
        
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('customer')->__('Order #'),
            'width'     => '100',
            'index'     => 'increment_id',
            'filter_index' => 'order.increment_id',
            'align' => 'center',
            'renderer'=>'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Order'
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('customer')->__('Purchase On'),
            'index'     => 'created_at',
            'filter_index'=> 'order.created_at',
            'type'      => 'datetime',
        ));

        $this->addColumn('sold_at', array(
            'header'    => Mage::helper('customer')->__('Sold On'),
            'index'     => 'sold_at',
            'type'      => 'datetime',
        ));

        /*$this->addColumn('shipping_firstname', array(
            'header'    => Mage::helper('customer')->__('Shipped to First Name'),
            'index'     => 'shipping_firstname',
        ));

        */
        $this->addColumn('Product', array(
            'header'    => Mage::helper('customer')->__('Product Name'),
            'index'     => 'name',
            'filter_index'=> 'name',
        ));

        $this->addColumn('Shipping', array(
            'header'    => Mage::helper('customer')->__('Shipping From'),
            'index'     => 'shipping_name',
            'filter_index' => 'order_grid.shipping_name'
        ));

        $this->addColumn('Quantity', array(
            'header'    => Mage::helper('customer')->__('Quantity'),
            'index'     => 'qty_main',
            'filter_index' => 'main_table.qty',
            // 'type'      =>'number',
            'align' => 'center',
        ));

        $priceColumnOptions = Mage::helper('portfolio')->getPriceColumnOptions();

        $this->addColumn('buy_price', $priceColumnOptions['buy_price']);

        $this->addColumn('sold_price', $priceColumnOptions['sold_price']);

        $this->addColumn('bid_price_cus', $priceColumnOptions['bid_price_cus']);
        $this->addColumn('bid_price_sure', $priceColumnOptions['bid_price_sure']);

        $this->addColumn('ask_price', $priceColumnOptions['ask_price']);
        
        $this->addColumn('market_price', $priceColumnOptions['market_price']);

        $this->addColumn('ROI', array(
            'header'    => Mage::helper('customer')->__('% Return'),
            'renderer' => 'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_ROI',
            'align' => 'center',
            'filter'    => false,
        ));

        $this->addColumn('action', array(
            'header'    => 'actions',
            'type'      => 'action',
            'getter'     => 'getIdMain',
            'actions'   => array(
                    array(
                        'caption' => Mage::helper('customer')->__('Edit'),
                        'url'     => array('base'=>'adminhtml/portfolio_index/edit'),
                        'field'   => 'id',
                        // 'confirm' => 'Are you sure to sell this portfolio?',
                        // 'popup'   => true
                        'onclick'  => 'showWindow(jQuery(this).attr("href"));return false;'
                    )
                ),
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
            'align' => 'center',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/winesold', array('_current' => true));
    }
  }