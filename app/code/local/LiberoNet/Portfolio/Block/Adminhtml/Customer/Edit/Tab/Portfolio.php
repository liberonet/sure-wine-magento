<?php
class LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Portfolio extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
    {
        parent::__construct();
        $this->setId('portfolio_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
    }

    protected function _prepareLayout(){
        $this->setChild('add_portfolio_button',
            $this->getLayout()->createBlock('adminhtml/widget_button')
                ->setData(array(
                    'label'     => Mage::helper('adminhtml')->__('Add Portfolio'),
                    'onclick'   => 'showWindow(\'/admin/portfolio_index/add/id/'.$this->getRequest()->getParam('id').'\',\'Add Portfolio\')',
                    'class'   => 'task'
                ))
        );
        return parent::_prepareLayout();
    }
    
    public function getAddPortfolioButtonHtml()
    {
        return $this->getChildHtml('add_portfolio_button');
    }

    public function getMainButtonsHtml()
    {
        $html = '';
        if($this->getFilterVisibility()){
            $html.= $this->getResetFilterButtonHtml();
            $html.= $this->getSearchButtonHtml();
            // $html.= $this->getAddPortfolioButtonHtml();
        }
        return $html;
    }
    protected function _prepareCollection()
    {
      $collection = Mage::getResourceModel("portfolio/portfolio_collection")
                    ->addFieldToSelect('*')
                    ->addFieldToSelect('id_libero_portfolio','id_main')
                    ->addFieldToSelect('status','status_main')
                    ->addFieldToSelect('qty','qty_main')
                    ->addFieldToFilter('main_table.status','portfolio')
                    ->addFieldToFilter('customer_id',Mage::registry('current_customer')->getId())
                    ->join(array('item'=>'sales/order_item'),'main_table.item_id=item.item_id',array('order_id','base_price'))
                    ->join(array('order'=>'sales/order_grid'),'item.order_id=order.entity_id',array('base_currency_code','order_currency_code','shipping_name','created_at','increment_id','entity_id'))
                    ->join(array('product'=>'catalog/product'),'main_table.product_id=product.entity_id',array('entity_id'))
                    //->setOrder('created_at', 'desc')
                    ;

                    $prodNameAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'name')
                                    ->getAttributeId();
                    
                    $collection = Mage::helper('portfolio')->getPricesCollection($collection,'product');

                    $collection->getSelect()
                                        ->joinLeft(
                                        array('cpev' => 'catalog_product_entity_varchar'), 
                                        'cpev.entity_id=product.entity_id AND cpev.attribute_id='.$prodNameAttrId.'', 
                                        array('name' => 'value')
                                        );
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('customer')->__('Order #'),
            'width'     => '100',
            'index'     => 'increment_id',
            'filter_idnex' => 'order.increment_id',
            'align' => 'center',
            'renderer'=>'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Order'
        ));

        $this->addColumn('created_at', array(
            'header'    => Mage::helper('customer')->__('Purchase On'),
            'index'     => 'created_at',
            'filter_index'=> 'order.created_at',
            'type'      => 'datetime',
        ));

        /*$this->addColumn('shipping_firstname', array(
            'header'    => Mage::helper('customer')->__('Shipped to First Name'),
            'index'     => 'shipping_firstname',
        ));

        */
        $this->addColumn('Product', array(
            'header'    => Mage::helper('customer')->__('Product Name'),
            'index'     => 'name',
            'align' => 'center',
        ));

        $this->addColumn('Quantity', array(
            'header'    => Mage::helper('customer')->__('Quantity'),
            'index'     => 'qty_main',
            'filter_index' => 'main_table.qty',
            'align' => 'center',
        ));

        $priceColumnOptions = Mage::helper('portfolio')->getPriceColumnOptions();

        $this->addColumn('bid_price_cus', $priceColumnOptions['bid_price_cus']);
        $this->addColumn('bid_price_sure', $priceColumnOptions['bid_price_sure']);

        $this->addColumn('ask_price', $priceColumnOptions['ask_price']);
        
        $this->addColumn('market_price', $priceColumnOptions['market_price']);

        $this->addColumn('customer_price', $priceColumnOptions['customer_price']);
        
        $this->addColumn('Shipping', array(
            'header'    => Mage::helper('customer')->__('Ship To'),
            'index'     => 'shipping_name',
        ));

        $this->addColumn('ROI', array(
            'header'    => Mage::helper('customer')->__('% Return'),
            'renderer' => 'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_ROI',
            'align' => 'center',
            'filter'    => false,
        ));

        $this->addColumn('action', array(
            'header'    => 'actions',
            'type'      => 'action',
            'getter'     => 'getIdMain',
            'actions'   => array(
                    array(
                        'caption' => Mage::helper('customer')->__('Edit'),
                        'url'     => array('base'=>'adminhtml/portfolio_index/edit'),
                        'field'   => 'id',
                        // 'confirm' => 'Are you sure to sell this portfolio?',
                        // 'popup'   => true
                        'onclick'  => 'showWindow(jQuery(this).attr("href"),\'Edit Portfolio\');return false;'
                    )
                ),
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
            'align' => 'center',
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('*/*/portfolios', array('_current' => true));
    }
  }