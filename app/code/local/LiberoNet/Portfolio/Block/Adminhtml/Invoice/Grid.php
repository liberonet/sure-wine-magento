<?php
class LiberoNet_Portfolio_Block_Adminhtml_Invoice_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
    {
        parent::__construct();
        $this->setId('invoice_wine_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
      $collection = Mage::getResourceModel("sales/order_invoice_item_collection")
                    ->addFieldToSelect('*')
                    ->addFieldToFilter('product_id',array('nin'=>Mage::helper('portfolio')->getExcludeArray()))
                    ->addFieldToFilter('price',array('gt'=>0))
                    ->join(array('product'=>'catalog/product'),'main_table.product_id=product.entity_id',array('product_id'=>'entity_id','sku'))
                    ->join(array('invoice'=>'sales/invoice'),'main_table.parent_id=invoice.entity_id',array('order_currency_code','base_currency_code','created_at','increment_id','invoice_id'=>'entity_id'))
                    ->join(array('order'=>'sales/order'),'order.entity_id=invoice.order_id','customer_id')
                    ->join(array('customer'=>'customer/entity'),'order.customer_id=customer.entity_id','group_id')
                    //->setOrder('invoice.created_at', 'desc')
                    ;
                    $prodNameAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'name')
                                    ->getAttributeId();
                    $prodVintageAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'vintage')
                                    ->getAttributeId();
                    $prodCaseSizeAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'case_sizes')
                                    ->getAttributeId();
                    $prodBottleSizeAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'bottle_size')
                                    ->getAttributeId();
                    $fn = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
                    $ln = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');
        $collection->getSelect()
                    ->joinLeft(
                    array('cpev2' => 'catalog_product_entity_varchar'), 
                    'cpev2.entity_id=product_id AND cpev2.attribute_id='.$prodNameAttrId.'', 
                    array('name' => 'value')
                    )
                    ->joinLeft(
                    array('cpev3' => 'catalog_product_entity_int'), 
                    'cpev3.entity_id=product_id AND cpev3.attribute_id='.$prodCaseSizeAttrId.'', 
                    array('case_size' => 'value')
                    )
                    ->join(array('ce1' => 'customer_entity_varchar'), 'ce1.entity_id=order.customer_id', array('firstname' => 'value'))
                    ->where('ce1.attribute_id='.$fn->getAttributeId()) 
                    ->join(array('ce2' => 'customer_entity_varchar'), 'ce2.entity_id=order.customer_id', array('lastname' => 'value'))
                    ->where('ce2.attribute_id='.$ln->getAttributeId()) 
                    ->columns(new Zend_Db_Expr("CONCAT(`ce1`.`value`, ' ',`ce2`.`value`) AS customer_name"));
                    $this->setCollection($collection);

                    // var_dump($collection->getSelect()->__toString());exit;
        $this->addExportType('adminhtml/portfolio_invoice/exportCsv', Mage::helper('sales')->__('CSV'));
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        // if(!Mage::registry('current_customer')){
            $this->addColumn('customer_name', array(
                'header'    => Mage::helper('customer')->__('Customer'),
                'width'     => '100',
                'index'     => 'customer_name',
                'filter_index' => "CONCAT(`ce1`.`value`, ' ',`ce2`.`value`)",
                'align' => 'center',
                'renderer'=>'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Customer'
            ));
        // }
        $groups = Mage::getResourceModel('customer/group_collection')
            ->addFieldToFilter('customer_group_id', array('gt'=> 0))
            ->load()
            ->toOptionHash();

        $this->addColumn('group', array(
            'header'    =>  Mage::helper('customer')->__('Group'),
            'width'     =>  '100',
            'index'     =>  'group_id',
            'filter_index' => 'customer.group_id',
            'type'      =>  'options',
            'options'   =>  $groups,
        ));

        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('customer')->__('Invoice #'),
            'width'     => '100',
            'index'     => 'increment_id',
            'filter_index' => 'invoice.increment_id',
            'align' => 'center',
            'renderer'=>'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Invoice'
        ));

        // $this->addColumn('created_at', array(
        //     'header'    => Mage::helper('customer')->__('Purchase On'),
        //     'index'     => 'created_at',
        //     'filter_index'=> 'order.created_at',
        //     'type'      => 'datetime',
        // ));

        $this->addColumn('sold_at', array(
            'header'    => Mage::helper('customer')->__('Sold On'),
            'index'     => 'created_at',
            'filter_index' => 'invoice.created_at',
            'type'      => 'datetime',
        ));

        /*$this->addColumn('shipping_firstname', array(
            'header'    => Mage::helper('customer')->__('Shipped to First Name'),
            'index'     => 'shipping_firstname',
        ));

        */
        $this->addColumn('Product', array(
            'header'    => Mage::helper('customer')->__('Product SKU'),
            'index'     => 'sku',
            'filter_index'=> 'product.sku',
        ));

        $this->addColumn('Product_name', array(
            'header'    => Mage::helper('customer')->__('Product Name'),
            'index'     => 'name',
            'filter_index'=> 'name',
        ));

        $this->addColumn('Quantity', array(
            'header'    => Mage::helper('customer')->__('Quantity'),
            'index'     => 'qty',
            'filter_index' => 'main_table.qty',
            'type'      =>'number',
            'align' => 'center',
        ));

        $this->addColumn('Currency', array(
            'header'    => Mage::helper('customer')->__('Currency'),
            'index'     => 'order_currency_code',
            'filter_index' => 'invoice.order_currency_code',
            // 'type'      =>'number',
            'align' => 'center',
        ));
        
        $this->addColumn('total', array(
            'header'    => Mage::helper('customer')->__('Amount'),
            'index'     => 'row_total',
            'filter_index'=>'main_table`.`row_total`',
            'type'      => 'currency',
            'currency'  => 'order_currency_code',
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        ));

        $this->addColumn('local_total', array(
            'header'    => Mage::helper('customer')->__('Local Amount'),
            'index'     => 'base_row_total',
            'filter_index'=>'main_table`.`base_row_total`',
            'type'      => 'currency',
            'currency'  => 'base_currency_code',
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        ));

        // $this->addColumn('sure_selling_price', array(
        //     'header'    => Mage::helper('customer')->__('Sure Selling'),
        //     'index'     => 'price',
        //     'filter_index'=>'price',
        //     'type'      => 'currency',
        //     'currency'  => 'base_currency_code',
        //     'align'     => 'center',
        //     'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        // ));

        // $this->addColumn('sure_total', array(
        //     'header'    => Mage::helper('customer')->__('Sure Total'),
        //     'index'     => 'sure_total',
        //     'filter_index'=>'ROUND(cpev1.value*0.95*`main_table`.`qty`,2)',
        //     'type'      => 'currency',
        //     'currency'  => 'base_currency_code',
        //     'align'     => 'center',
        //     'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        // ));


        if($this->_isExport)$this->setCountTotals(true);
        return parent::_prepareColumns();
    }

    public function getGridUrl()
    {
        return $this->getUrl('adminhtml/portfolio_invoice/wine', array('_current' => true));
    }

    public function getTotals()
    {
        $totals = new Varien_Object();
        $fields = array(
            'base_row_total' => 0, //actual column index, see _prepareColumns()
            'row_total' => 0,
        );
        foreach ($this->getCollection() as $item) {
            foreach($fields as $field=>$value){
                $fields[$field]+=$item->getData($field);
            }
        }
        //First column in the grid
        $fields['customer_name']='Totals';
        $totals->setData($fields);
        return $totals;
    }
  }