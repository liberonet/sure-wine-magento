<?php
class LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
  public function __construct()
    {
        parent::__construct();
        $this->setId('portfolio_selling_grid');
        $this->setDefaultSort('created_at', 'desc');
        $this->setUseAjax(true);
    }

    protected function _prepareCollection()
    {
      $collection = Mage::getResourceModel("portfolio/portfolio_collection")
                    ->addFieldToSelect('*')
                    ->addFieldToSelect('sold_at')
                    ->addFieldToSelect('receiving_price')
                    ->addFieldToSelect('id_libero_portfolio','id_main')
                    ->addFieldToSelect('status','status_main')
                    ->addFieldToSelect('qty','qty_main')
                    ->addFieldToSelect('customer_id')
                    ->addFieldToSelect('stage')
                    ->addFieldToFilter('main_table.status','pending');
        if(Mage::registry('current_customer')) $collection->addFieldToFilter('customer_id',Mage::registry('current_customer')->getId());
        $collection->join(array('item'=>'sales/order_item'),'main_table.item_id=item.item_id',array('order_id','base_price'))
                    ->join(array('product'=>'catalog/product'),'main_table.product_id=product.entity_id',array('entity_id','sku'))
                    ->join(array('order'=>'sales/order'),'item.order_id=order.entity_id',array('base_currency_code','created_at','increment_id','entity_id'))
                    ->join(array('order_grid'=>'sales/order_grid'),'order_grid.increment_id=order.increment_id','shipping_name')
                    //->setOrder('order.created_at', 'desc')
                    ;
                    // $prodPriceAttrId = Mage::getModel('eav/entity_attribute')
                    //                 ->loadByCode('4', 'price')
                    //                 ->getAttributeId();
                    $prodNameAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'name')
                                    ->getAttributeId();
                    $prodVintageAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'vintage')
                                    ->getAttributeId();
                    $prodCaseSizeAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'case_sizes')
                                    ->getAttributeId();
                    $prodBottleSizeAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'bottle_size')
                                    ->getAttributeId();
                    $fn = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'firstname');
                    $ln = Mage::getModel('eav/entity_attribute')->loadByCode('1', 'lastname');
                    $collection = Mage::helper('portfolio')->getPricesCollection($collection,'product');
        $collection->getSelect()
                    ->joinLeft(array('credit'=>'libero_flat_credit'),'main_table.credit_id=credit.entity_id','shipping_from')
                    // ->joinLeft(
                    // array('cpev1' => 'catalog_product_entity_decimal'), 
                    // 'cpev1.entity_id=product.entity_id AND cpev1.attribute_id='.$prodPriceAttrId.'', 
                    // array('ask_price'=>'cpev1.value','bid_price' => 'ROUND((cpev1.value/1.05)*0.9,2)')
                    // )
                    ->joinLeft(
                    array('cpev2' => 'catalog_product_entity_varchar'), 
                    'cpev2.entity_id=product.entity_id AND cpev2.attribute_id='.$prodNameAttrId.'', 
                    array('name' => 'value')
                    )
                    ->joinLeft(
                    array('cpev3' => 'catalog_product_entity_int'), 
                    'cpev3.entity_id=product.entity_id AND cpev3.attribute_id='.$prodCaseSizeAttrId.'', 
                    array('case_size' => 'value')
                    )
                    ->joinLeft(
                    array('cpev4' => 'catalog_product_entity_varchar'), 
                    'cpev4.entity_id=product.entity_id AND cpev4.attribute_id='.$prodBottleSizeAttrId.'', 
                    array('bottle_size' => 'value')
                    )
                    ->joinLeft(
                    array('cpev5' => 'catalog_product_entity_varchar'), 
                    'cpev5.entity_id=product.entity_id AND cpev5.attribute_id='.$prodVintageAttrId.'', 
                    array('vintage' => 'value')
                    )
                    ->join(array('ce1' => 'customer_entity_varchar'), 'ce1.entity_id=main_table.customer_id', array('firstname' => 'value'))
                    ->where('ce1.attribute_id='.$fn->getAttributeId()) 
                    ->join(array('ce2' => 'customer_entity_varchar'), 'ce2.entity_id=main_table.customer_id', array('lastname' => 'value'))
                    ->where('ce2.attribute_id='.$ln->getAttributeId()) 
                    ->columns(new Zend_Db_Expr("CONCAT(`ce1`.`value`, ' ',`ce2`.`value`) AS customer_name"))
                    ->columns(new Zend_Db_Expr("receiving_price*`main_table`.`qty` AS total"));
                    // $collection->addItem(new Varien_Object(array('id'=>9999,'customer_name'=>'trans')));
                    $this->setCollection($collection);
                    //var_dump($collection->getSelect()->__toString());
        $this->addExportType('adminhtml/portfolio_index/exportCsv', Mage::helper('sales')->__('CSV'));
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('id', array(
            'header'    => Mage::helper('customer')->__('ID'),
            'width'     => '100',
            'index'     => 'id_main',
            'filter_index' => 'id_libero_portfolio',
            'align' => 'center'
        ));
        if(!Mage::registry('current_customer')){
            $this->addColumn('customer_name', array(
                'header'    => Mage::helper('customer')->__('Customer'),
                'width'     => '100',
                'index'     => 'customer_name',
                'filter_index' => "CONCAT(`ce1`.`value`, ' ',`ce2`.`value`)",
                'align' => 'center',
                'renderer'=>'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Customer'
            ));
        }

        $this->addColumn('increment_id', array(
            'header'    => Mage::helper('customer')->__('Order #'),
            'width'     => '100',
            'index'     => 'increment_id',
            'filter_index' => 'order.increment_id',
            'align' => 'center',
            'renderer'=>'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Order'
        ));

        // $this->addColumn('created_at', array(
        //     'header'    => Mage::helper('customer')->__('Purchase On'),
        //     'index'     => 'created_at',
        //     'filter_index'=> 'order.created_at',
        //     'type'      => 'datetime',
        // ));

        $this->addColumn('sold_at', array(
            'header'    => Mage::helper('customer')->__('Sold On'),
            'index'     => 'sold_at',
            'type'      => 'datetime',
        ));

        /*$this->addColumn('shipping_firstname', array(
            'header'    => Mage::helper('customer')->__('Shipped to First Name'),
            'index'     => 'shipping_firstname',
        ));

        */
        $this->addColumn('Product', array(
            'header'    => Mage::helper('customer')->__('Product SKU'),
            'index'     => 'sku',
            'filter_index'=> 'product.sku',
        ));

        $this->addColumn('Product_name', array(
            'header'    => Mage::helper('customer')->__('Product Name'),
            'index'     => 'name',
            'filter_index'=> 'name',
        ));

        $this->addColumn('location', array(
            'header' => Mage::helper('customer')->__('Location'),
            'index' => 'shipping_name',
            'column_css_class'=>'no-display',//this sets a css class to the column row item
            'header_css_class'=>'no-display',//this sets a css class to the column header
        ));

        $this->addColumn('Vintage', array(
            'header'    => Mage::helper('customer')->__('Vintage'),
            'index'     => 'vintage',
            'filter_index' => 'cpev5.value',
        ));

        $this->addColumn('format', array(
            'header'    => Mage::helper('customer')->__('Format'),
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Format',
            'filter'    => false,
        ));

        $this->addColumn('Quantity', array(
            'header'    => Mage::helper('customer')->__('Quantity'),
            'index'     => 'qty_main',
            'filter_index' => 'main_table.qty',
            // 'type'      =>'number',
            'align' => 'center',
        ));
        
        $this->addColumn('Total_bottles', array(
            'header'    => Mage::helper('customer')->__('Total Bottles'),
            // 'index'     => 'qty_main',
            // 'filter_index' => 'main_table.qty',
            // 'type'      =>'number',
            'align' => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Total',
            'column_css_class'=>'no-display',//this sets a css class to the column row item
            'header_css_class'=>'no-display',//this sets a css class to the column header
        ));

        $priceColumnOptions = Mage::helper('portfolio')->getPriceColumnOptions();

        $this->addColumn('bid_price_cus', $priceColumnOptions['bid_price_cus']);
        $this->addColumn('bid_price_sure', $priceColumnOptions['bid_price_sure']);

        $this->addColumn('bid_price_euro', $priceColumnOptions['bid_price_euro']);

        $this->addColumn('ask_price', $priceColumnOptions['ask_price']);
        
        $this->addColumn('market_price', $priceColumnOptions['market_price']);

        $this->addColumn('cus_selling_price', $priceColumnOptions['cus_selling_price']);


        $this->addColumn('cus_total', array(
            'header'    => Mage::helper('customer')->__('Customer Total'),
            'index'     => 'total',
            'filter_index'=>'ROUND(receiving_price*`main_table`.`qty`,2)',
            'type'      => 'currency',
            'currency'  => 'base_currency_code',
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        ));

        // $this->addColumn('sure_selling_price', array(
        //     'header'    => Mage::helper('customer')->__('Sure Selling'),
        //     'index'     => 'price',
        //     'filter_index'=>'price',
        //     'type'      => 'currency',
        //     'currency'  => 'base_currency_code',
        //     'align'     => 'center',
        //     'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        // ));

        // $this->addColumn('sure_total', array(
        //     'header'    => Mage::helper('customer')->__('Sure Total'),
        //     'index'     => 'sure_total',
        //     'filter_index'=>'ROUND(cpev1.value*0.95*`main_table`.`qty`,2)',
        //     'type'      => 'currency',
        //     'currency'  => 'base_currency_code',
        //     'align'     => 'center',
        //     'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        // ));

        $this->addColumn('ROI', array(
            'header'    => Mage::helper('customer')->__('% Return'),
            'renderer' => 'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_ROI',
            'align' => 'center',
            'filter'    => false,
        ));

        $this->addColumn('stage', array(
            'header'    => Mage::helper('customer')->__('Stage'),
            'index' => 'stage',
            'renderer' => 'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_Stage',
            'align' => 'center',
            'filter'    => false,
        ));
        $this->addColumn('action', array(
            'header'    => 'actions',
            'type'      => 'action',
            'getter'     => 'getIdMain',
            'actions'   => array(
                    array(
                        'caption' => Mage::helper('customer')->__('Edit'),
                        'url'     => array('base'=>'adminhtml/portfolio_index/edit'),
                        'field'   => 'id',
                        // 'confirm' => 'Are you sure to sell this portfolio?',
                        // 'popup'   => true
                        'onclick'  => 'showWindow(jQuery(this).attr("href"),\'Edit Portfolio\');return false;'
                    )
                ),
            'filter'    => false,
            'sortable'  => false,
            'is_system' => true,
            'align' => 'center',
        ));

        if($this->_isExport)$this->setCountTotals(true);

        return parent::_prepareColumns();
    }

    public function getTotals()
    {
        $totals = new Varien_Object();
        $fields = array(
            'total' => 0, //actual column index, see _prepareColumns()
        );
        foreach ($this->getCollection() as $item) {
            foreach($fields as $field=>$value){
                $fields[$field]+=$item->getData($field);
            }
        }
        $total = round($fields[$field],2);
        $fields[$field] = 'Total:'.$total;
        $fields[$field] .= "\r Less 1% Trans Fee: ".round($total*0.01,2);
        $fields[$field] .= "\r Net Proceeds of Sale: ".round($total*0.99,2);
        //First column in the grid
        $fields['customer_name']='Totals';
        $totals->setData($fields);
        return $totals;
    }

    public function getGridUrl()
    {
        return $this->getUrl('adminhtml/portfolio_index/wineselling', array('_current' => true));
    }

  }
