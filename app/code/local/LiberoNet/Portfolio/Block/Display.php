<?php
class LiberoNet_Portfolio_Block_Display extends Mage_Core_Block_Template
{
     public function __construct()
    {
        parent::__construct();
        $this->setTemplate("portfolio/display.phtml");
        $portfolios = Mage::getResourceModel("portfolio/portfolio_collection")
                ->addFieldToSelect('*')
                ->setPageSize(false)
                ->addFieldToFilter('customer_id',Mage::getSingleton('customer/session')->getCustomer()->getId())
                ->setOrder('created_at', 'desc');

        $this->setPorts($portfolios);

    }

    protected function _prepareLayout()
    {
        // $pager = $this->getLayout()->createBlock('page/html_pager', 'liberonet.portfolio.display.pager')
        //     ->setCollection($this->getPorts());
        // $this->setChild('pager', $pager);
        $this->getPorts()->load();
        return $this;
    }

    public function getTotals(){
        return Mage::getModel('portfolio/portfolio')->getTotals(Mage::getSingleton('customer/session')->getCustomer()->getId());
    }

    public function getPagerHtml()
    {
        return $this->getChildHtml('pager');
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

}