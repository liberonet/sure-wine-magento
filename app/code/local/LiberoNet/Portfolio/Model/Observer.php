<?php
class LiberoNet_Portfolio_Model_Observer extends Varien_Event_Observer
{
   public function __construct()
   {
   }

   public function orderCompleteObserver($observer)
   {
         $invoice = $observer->getEvent()->getInvoice();
	        if( $invoice->getState() == Mage_Sales_Model_Order_Invoice::STATE_PAID){

	        	$order_id = $invoice->getOrderId();
	        	$order = Mage::getModel('sales/order')->load($order_id);
	        	$port_flag = false;
	        	foreach($order->getAllItems() as $item){
	        		if(Mage::helper('portfolio')->isStorage($item->getProductId())){
	        			$port_flag = true;
	        			break;
	        		}
	        	}
	        	if($port_flag) 
	        		Mage::getModel('portfolio/portfolio')->createPortfolioFromOrder($order);
	    }
	    return $this;
 }

 public function portfolioSellObserver($observer)
  {
          
         	$port = $observer->getEvent()->getPort();
    	      $templateId = Mage::getStoreConfig('sales_email/portfolio/sale_template');
        	
          $receivers = Mage::getStoreConfig('sales_email/portfolio/copy_to', $this->getStoreId());
          
            $customer = Mage::getModel('customer/customer')->load($port->getCustomerId());
            $product = Mage::getModel('catalog/product')->load($port->getProductId());
            $templateParams = array(
                  'customer_name'        => $customer->getName(),
                  'price'                => $port->getReceivingPrice(),
                  'product'              => $product->getName(),
                  'qty'                  => $port->getQty()
              );
          $this->sendPortfolioNotification($receivers,$templateId,$templateParams);
	    return $this;
 }

 private function sendPortfolioNotification($receivers, $templateId, $templateParams){
          $storeId = Mage::app()->getStore()->getId();
          $mailer = Mage::getModel('core/email_template_mailer');
          if(!empty($receivers)){
            $recievers_array = explode(',',$receivers);
            Mage::log($recievers_array);
            foreach($recievers_array as $receiver){
              $emailInfo = Mage::getModel('core/email_info');
              $emailInfo->addTo($receiver);
              $mailer->addEmailInfo($emailInfo);
              }
          }
          // Set all required params and send emails
          $mailer->setSender(Mage::getStoreConfig('sales_email/portfolio/identity', $storeId));
          $mailer->setStoreId($storeId);
          $mailer->setTemplateId($templateId);
          $mailer->setTemplateParams(
            $templateParams
          );
          $mailer->send();

 }
 public function portfolioChangeStageObserver($observer){
          $port = $observer->getEvent()->getPort();
          $stage = $port->getStage();
          $receivers = Mage::getStoreConfig('sales_email/portfolio/'.$stage, $this->getStoreId());
          if($receivers){
            $templateId = Mage::getStoreConfig('sales_email/portfolio/stage_template');
            $customer = Mage::getModel('customer/customer')->load($port->getCustomerId());
            $product = Mage::getModel('catalog/product')->load($port->getProductId());
            $stage_arr = Mage::helper('portfolio')->getStages();
            $templateParams = array(
              'id'=>$port->getId(),
              'stage'=>$stage_arr[$port->getStage()]
              );
          $this->sendPortfolioNotification($receivers,$templateId,$templateParams);
          }
      return $this;
 }
}
?>