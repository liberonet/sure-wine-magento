<?php
class LiberoNet_Portfolio_Model_Portfolio extends Mage_Core_Model_Abstract
{
     public function _construct()
     {
         parent::_construct();
         $this->_init('portfolio/portfolio');
     }

     public function getPortfolios(){
     	
     }

     public function getTotals($customer_id){
     	if($customer_id){
     		$items = $this->getCollection()
     				->clear()
     				->setPageSize(false)
     				->addFieldToFilter('customer_id',$customer_id)
     				->addFieldToFilter('status','portfolio')
     				->getItems();
     		foreach ($items as $item) {
     			$product = Mage::getModel('catalog/product')->load($item->getProductId());
     			$order_item = Mage::getModel('sales/order_item')->load($item->getItemId());
     			if(Mage::helper('portfolio')->isWine($item->getProductId())){
     			     				$total_value += $product->getPrice()*$item->getQty();
     			     				$total_original_value += $order_item->getBasePrice() * $item->getQty();
     			     			}
     		}
     		return array('value' => $total_value, 'original'=>$total_original_value);
     	}
     }

     public function createPortfolioFromOrder($order){
		$write = Mage::getSingleton('core/resource')->getConnection('core_write'); 
		$order_items = $order->getAllItems();
        $configurables = array();
		foreach($order_items as $order_item){
			if(Mage::helper('portfolio')->isWine($order_item->getProductId())&&(!in_array($order_item->getProductId(),$configurables))){
            if($order_item->getProductType()=="configurable")continue;
            
            $data = array();
            $data['product_id'] = $order_item->getProductId();
            if($order_item->getParentItemId() !== null){
                $parent_item = $order->getItemById($order_item->getParentItemId());
                $data['item_id'] = $parent_item->getId();
            }else{
                $data['item_id'] = $order_item->getId();
            }

			$data['status'] = 'portfolio';
			$data['qty'] = $order_item->getQtyOrdered();
			$data['created_at'] = $order->getCreatedAt();
			$data['customer_id'] = $order->getCustomerId();
			$write->insert("libero_portfolio", $data); 
			}
	     }
	 }

	 public function sell($id,$price,$qty,$date){
	 	$this->load($id);
        $status = 0;
        $message = '';
        $ask_price = Mage::getModel('catalog/product')->load(($this->getProductId()))->getPrice();
        $max_price = Mage::helper('portfolio')->getBiddingPrice($ask_price);
        if($max_price >= $price){

        if(!$date)$date=Varien_Date::now();
        if($this->getStatus()=="portfolio"){
            if(intval($qty) == $this->getQty()){
                $this->setStatus('pending');
                $this->setSoldAt($date);
                $this->setReceivingPrice($price);
                $this->save();
                $new_port = $this;
                $status = 1;
            }
            else if(intval($qty) < $this->getQty()){
                $new_port = Mage::getModel('portfolio/portfolio');
                $new_port->setQty($qty);
                $this->setQty($this->getQty()-$qty);
                $new_port->setStatus('pending');
                $new_port->setCustomerId($this->getCustomerId());
                $new_port->setProductId($this->getProductId());
                $new_port->setCreatedAt($this->getCreatedAt());
                $new_port->setSoldAt($date);
                $new_port->setItemId($this->getItemId());
                $new_port->setReceivingPrice($price);
                $new_port->save();
                $this->save();
                $status = 1;
                }
            else{
                $status = 0;
                $message = 'invalid quantity';
            }
            if($status){
                Mage::dispatchEvent('portfolio_sell', array('port'=>$new_port));
            }
         }else if($this->getStatus()=="pending"){
                $this->setQty($qty);
                $this->setSoldAt($date);
                $this->setReceivingPrice($price);
                $this->save();
                $status = 1;
         }
     }else{
        $message = 'Price must be below £'.$max_price.'(bid price)';
     }

         return array('status'=>$status,'message'=>$message);
	 }

     public function completeSell($id,$date,$rma_ref = null,$shipping_from=null,$commision=null){
        $this->load($id);
        $status = 0;
        $message = '';
        if($this->getStatus()=="pending"){
        if(!$rma_ref){
                $rma = mage::getModel('ProductReturn/Rma');
                $order = Mage::getModel('sales/order')->load(Mage::getModel('sales/order_item')->load($this->getItemId())->getOrderId());
                $rma->setrma_order_id($order->getId());
                $rma->setrma_customer_id($order->getcustomer_id());
                $rma->setrma_customer_name($rma->getCustomer()->getName());
                $rma->setrma_customer_phone($order->getShippingAddress()->gettelephone());
                $rma->setrma_customer_email($rma->getCustomer()->getemail());
                $rma->setrma_ref(mage::helper('ProductReturn')->createRmaReference($rma));
                $rma->setrma_address_id($order->getshipping_address_id());
                $data = array();
                $rma->setrma_is_locked("");
                $rma->setrma_status("requested");
                $rma->setrma_manager_id(Mage::getSingleton('admin/session')->getUser()->getId());
                $rma->setrma_expire_date("");
                $rma->setrma_reception_date("");
                $rma->setrma_return_date("");
                $rma->setrma_created_at(date('Y-m-d H:n'));
                $rma->setrma_updated_at(date('Y-m-d H:n'));
                $item_id = $this->getItemId();
                $data['rp_qty_'.$item_id] = $this->getQty();
                $data['rp_reason_'.$item_id] = 'Wine Sold';
                $data['rp_reason_'.$item_id] = 'Cancel';
                $data['rp_request_type_'.$item_id] = 'Refund';
                $rmaProducts = $rma->getProducts();
                $rma->save();
                $rma_ref = $rma->getrma_ref();
                //set sub products information
                foreach ($rmaProducts as $rmaProduct) {
                    if (mage::getModel('ProductReturn/RmaProducts')->productIsDisplayed($rmaProduct)) {
                        $id  = $rmaProduct->getitem_id();
                        $rma_qty = 0;
                        if (isset($data['rp_qty_' . $id]))
                            $rma_qty = $data['rp_qty_' . $id];
                        else
                            $rma_qty = $rmaProduct->getrp_qty();
                        if (isset($data['rp_description_' . $id])) {
                            $description  = $data['rp_description_' . $id];
                            $serials      = $data['rp_serials_' . $id];
                            $reason       = $data['rp_reason_' . $id];
                            $request_type = $data['rp_request_type_' . $id];
                        } else {
                            $description  = '';
                            $serials      = '';
                            $reason       = '';
                            $request_type = '';
                        }
                        $rmaProductItem = $rma->updateSubProductInformation($rmaProduct, $rma_qty, $description, $reason, $serials, $request_type);
                    }
                }
                // $credit = Mage::getModel('credit/credit');
                // $customer = Mage::getModel('customer/customer')->load($this->getCustomerId());
                // $credit->setCustomerId($this->getCustomerId());
                // $quote = Mage::getModel('sales/quote')->setStoreId(Mage::app()->getStore('default')->getId());
                // $quote->assignCustomer($customer);
                // try{
                //     Mage::getModel('cataloginventory/stock')->backItemQty($this->getProductId(),$qty);
                // $item = $quote->addProduct(Mage::getModel('catalog/product')->load($this->getProductId()));
                // }catch(Exception $e){
                //     $message = 'product ID: '.$this->getProductId().' out of stock';
                //     return array('status'=>$status,'message'=>$message);
                // }
                // $item->setQuote($quote);
                // $primary_address = $customer->getPrimaryShippingAddress();
                // $firstname = $primary_address->getFirstname();
                // $lastname = $primary_address->getLastname();
                // $country_id = $primary_address->getCountryId();
                // $postcode = $primary_address->getPostCode();
                // $region_id = $primary_address->getRegionId();
                // $street = $primary_address->getStreet();
                // $city = $primary_address->getCity();
                // $telephone = $primary_address->getTelephone();
                // $addressData = array(
                // 'firstname' => $firstname,
                // 'lastname' => $lastname,
                // 'street' => $street,
                // 'city' => $city,
                // 'postcode' => $postcode,
                // 'telephone' => $telephone,
                // 'country_id' => $country_id,
                // 'region_id' =>$region_id , // id from directory_country_region table
                // );
                // $billingAddress = $quote->getBillingAddress()->addData($addressData);
                // $shippingAddress = $quote->getShippingAddress()->addData($addressData);
                // $shippingAddress->setCollectShippingRates(true)->collectShippingRates()
                // ->setShippingMethod('freeshipping_freeshipping')
                // ->setPaymentMethod('checkmo');
                // $quote->getPayment()->importData(array('method' => 'checkmo'));

                // $quote->collectTotals();
                // $quote->save();
                // $service = Mage::getModel('sales/service_quote', $quote);
                // $new_credit = Mage::getModel('sales/convert_quote')->addressToCredit($quote->getBillingAddress());
                // $new_credit->setBillingAddress(Mage::getModel('sales/convert_quote')->addressToCreditAddress($quote->getBillingAddress()));
                // $new_credit->setPayment(Mage::getModel('sales/convert_quote')->paymentToCreditPayment($quote->getPayment()));
                // $amount = $price*$qty;
                // $creditItem = Mage::getModel('sales/convert_quote')->itemToCreditItem($item);
                // $creditItem->setPrice($price);
                // $creditItem->setPriceInclTax($price);
                // $creditItem->setCustomPrice($price);
                // $creditItem->setBasePrice($price);
                // $creditItem->setBasePriceInclTax($price);
                // $creditItem->setQty($qty);
                // $creditItem->setRowTotal($amount);
                // $creditItem->setRowTotalInclTax($amount);
                // $creditItem->setBaseRowTotal($amount);
                // $creditItem->setBaseRowTotalInclTax($amount);
                // $new_credit->addItem($creditItem);
                // $new_credit->setSubtotal($amount);
                // $new_credit->setBaseSubtotal($amount);
                // $new_credit->setGrandTotal($amount+$commision);
                // $new_credit->setBaseGrandTotal($amount+$commision);
                // $new_credit->setBaseTotalDue($amount+$commision);
                // $new_credit->setTotalDue($amount+$commision);
                // $new_credit->setState('processing');
                // $new_credit->setStatus('processing');

                // if($shipping_from)$new_credit->setShippingFrom($shipping_from);
                // if($commision)$new_credit->setCommision($commision);
                // if(!$date)$date = Varien_Date::now();
                // $new_credit->setCreatedAt($date)->save();
                // $credit_id = $new_credit->getId();

            }
        if(!$rma_ref) return array('status'=>$status,'message'=>'invalid rma reference!');
        
            // if(intval($qty) == $this->getQty()){
                $this->setStatus('sold');
                $this->setSoldAt($date);
                // $this->setReceivingPrice($price);
                $this->setRmaReference($rma_ref);
                if($commision)
                    $this->setTransactionFee($commision);
                $this->save();
                $status = 1;
            // }
            // else if(intval($qty) < $this->getQty()){
            //     $new_port = Mage::getModel('portfolio/portfolio');
            //     $new_port->setQty($qty);
            //     $this->setQty($this->getQty()-$qty);
            //     $new_port->setStatus('sold');
            //     $new_port->setCustomerId($this->getCustomerId());
            //     $new_port->setProductId($this->getProductId());
            //     $new_port->setCreatedAt($this->getCreatedAt());
            //     $new_port->setSoldAt($date);
            //     $new_port->setItemId($this->getItemId());
            //     $new_port->setReceivingPrice($thprice);
            //     $new_port->setRmaReference($rma_ref);
            //     $new_port->save();
            //     $this->save();
            //     $status = 1;
            //     }
            // else{
            //     $message = 'invalid quantity';
            // }
         }else{
            $message = 'Already Sold!';
         }
         return array('status'=>$status,'message'=>$message);
     }

     public function adminDelete($id,$qty){
        $this->load($id);
        if(intval($qty) == $this->getQty()){
            $this->delete();
            $this->save();}
        else if(intval($qty) < $this->getQty()){
            $this->setQty($this->getQty()-$qty);
            $this->save();
            }
        else return false;
        return true;
     }


     public function deletePort($id){
        $this->load($id);
        $this->delete();
        return true;
     }

        public function cancelSell($id,$qty){
   		$this->load($id);
        $ret['status'] = 0;
        $ret['message'] = "";
   		if($this->getStatus()=="pending"){
            $old_port = Mage::getModel('portfolio/portfolio')->getCollection()
                            ->addFieldToFilter('item_id',$this->getItemId())
                            ->addFieldToFilter('status','portfolio');
            if($old_port->getSize()){
                $new_port = $old_port->getFirstItem();
                $old_qty = $new_port->getQty();
            }
            else{
                $new_port = Mage::getModel('portfolio/portfolio');
                $old_qty = 0;
            }
            if(intval($this->getQty())>=$qty){
                $new_port->setQty($old_qty+$qty);
                $this->setQty($this->getQty()-$qty);
                $new_port->setStatus('portfolio');
                $new_port->setCustomerId($this->getCustomerId());
                $new_port->setProductId($this->getProductId());
                $new_port->setCreatedAt($this->getCreatedAt());
                $new_port->setItemId($this->getItemId());
                $new_port->save();
                if($this->getQty()>0)
                    $this->save();
                else
                    $this->delete();
                $ret['status'] = 1;
            }else{
                $ret['message'] = 'Invalid Quantity!';
            }
	   	}
	   	return $ret;
	 }

    public function changeStage($id,$stage){
        $this->load($id);
        $ret['status'] = 0;
        $ret['message'] = "";
        $old_stage = $this->getStage();
        if($stage == $old_stage){
            $ret['message'] = 'No changes detected';
        }else{
            if(in_array($stage, array_keys(Mage::helper('portfolio')->getStages())))
                {
                    $this->setStage($stage);
                    $this->save();
                    $ret['status'] = 1;
                }
        }
        if($ret['status'])
            Mage::dispatchEvent('portfolio_change_stage', array('port'=>$this));
        return $ret;
    }
}