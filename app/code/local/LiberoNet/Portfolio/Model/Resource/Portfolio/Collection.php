<?php
class LiberoNet_Portfolio_Model_Resource_Portfolio_Collection extends Mage_Sales_Model_Resource_Order_Collection
 {
     public function _construct()
     {
         parent::_construct();
         $this->_init('portfolio/portfolio');
     }
}