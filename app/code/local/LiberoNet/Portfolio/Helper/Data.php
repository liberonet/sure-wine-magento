<?php
   class LiberoNet_Portfolio_Helper_Data extends Mage_Core_Helper_Abstract
   {

//	check if a product is Storage Fee(SKU 358-360)
    public function isStorage($id){
    	return in_array($id, array('358','359','360','374','375','376','379','380','381','382','383','384','385','386','387'));
    }

    public function getExcludeArray(){
        return array('358','359','360','361','362','363','364','365','366','367','368','369','370','371','372','373','374','375','376','377','378','379','380','381','382','383','384','385','386','387','399','400','401','433','434','435','436','437');
    }
//	check if a product is Wine according to id
    public function isWine($id){
    	return !in_array($id, $this->getExcludeArray());
    }

    public function getPriceFilters(){
        return array('ask_price'=>'cpev1.value',
                     'bid_price_cus' => 'ROUND((cpev1.value/1.05)*0.92,2)',
                     'bid_price_sure' => 'ROUND((cpev1.value/1.05)*0.97,2)',
                     'market_price'=>'ROUND((cpev1.value/1.05),2)');
    }
    public function getPricesCollection($collection,$product_table){
    	$prodPriceAttrId = Mage::getModel('eav/entity_attribute')
                                    ->loadByCode('4', 'price')
                                    ->getAttributeId();

        $filters = $this->getPriceFilters();
        $collection->getSelect()->joinLeft(
                    array('cpev1' => 'catalog_product_entity_decimal'), 
                    'cpev1.entity_id=`'.$product_table.'`.entity_id AND cpev1.attribute_id='.$prodPriceAttrId.'', 
                    $filters
                    );
        return $collection;
    }

    public function getPriceColumnOptions(){
        $price_filters = $this->getPriceFilters();
    	$currency_code = (string) Mage::getStoreConfig(Mage_Directory_Model_Currency::XML_PATH_CURRENCY_BASE);
    	$ret = array();
    	$ret['bid_price_cus'] = array(
            'header'    => Mage::helper('customer')->__('Bid Price(Customer)'),
            'index'     => 'bid_price_cus',
            'filter_index'=> $price_filters['bid_price_cus'],
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        );
        $ret['bid_price_sure'] = array(
            'header'    => Mage::helper('customer')->__('Bid Price(Sure)'),
            'index'     => 'bid_price_sure',
            'filter_index'=> $price_filters['bid_price_sure'],
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        );

        $ret['bid_price_euro']= array(
            'header'    => Mage::helper('customer')->__('Bid Price Euro'),
            'index'     => 'bid_price_cus',
            'filter_index'=> $price_filters['bid_price_cus'],
            'type'      => 'currency',
            'currency_code'  => 'EUR',
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Customer_Edit_Tab_Renderer_PriceEuro',
            // 'column_css_class'=>'no-display',//this sets a css class to the column row item
            // 'header_css_class'=>'no-display',//this sets a css class to the column header
        );

        $ret['ask_price'] = array(
            'header'    => Mage::helper('customer')->__('Ask Price'),
            'index'     => 'ask_price',
            'filter_index'=> $price_filters['ask_price'],
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        );
        
        $ret['market_price'] = array(
            'header'    => Mage::helper('customer')->__('Market Price'),
            'index'     => 'market_price',
            'filter_index'=> $price_filters['market_price'],
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        );

        $ret['cus_selling_price'] = array(
            'header'    => Mage::helper('customer')->__('Customer Price'),
            'index'     => 'receiving_price',
            'filter_index'=>'main_table.receiving_price',
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align'     => 'center',
            'renderer'  => 'LiberoNet_Portfolio_Block_Adminhtml_Portfolio_Renderer'
        );

        $ret['customer_price'] = array(
            'header'    => Mage::helper('customer')->__('Customer Price'),
            'index'     => 'base_price',
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align' => 'center',
        );

        $ret['buy_price'] = array(
            'header'    => Mage::helper('customer')->__('Buy Price'),
            'index'     => 'base_price',
            'filter_index'=>'item.base_price',
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align' => 'center',
        );

        $ret['sold_price'] = array(
            'header'    => Mage::helper('customer')->__('Sold Price'),
            'index'     => 'receiving_price',
            'filter_index'=>'main_table.receiving_price',
            'type'      => 'currency',
            'currency_code'  => $currency_code,
            'align'     => 'center'
        );
        return $ret;
    }

    public function getBiddingPrice($ask_price){
    	return round(($ask_price/1.05)*0.92,2);
    }

    public function getStages(){
        return array('client_sale'=>'Client Sale','checking_wine'=>'Checking Wine','checking_price'=>'Checking Price','client_conf'=>'Client Conf','marked'=>'Marked for Sale','offer_made'=>'Offer Made','pending_pmt'=>'Pending Pmt','pending_docs'=>'Pending Docs','pmt_received'=>'Pmt Received','wine_released'=>'Wine Released','pmt_released'=>'Pmt Released','updated'=>'Portfolio Updated');
    }
   }