<?php
class LiberoNet_Portfolio_Block_Display extends Mage_Core_Block_Template
{
     public function __construct()
    {
        parent::__construct();
        $this->setTemplate("portfolio/display.phtml");
        $portfolios = Mage::getResourceModel("portfolio/portfolio_collection")->addFieldToSelect('*')->setOrder('created_at', 'desc');
        $orders = Mage::getResourceModel('sales/order_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('customer_id', Mage::getSingleton('customer/session')->getCustomer()->getId())
            ->addFieldToFilter('state', array('in' => Mage::getSingleton('sales/order_config')->getVisibleOnFrontStates()))
            ->setOrder('created_at', 'desc')
        ;
        $this->setPorts($portfolios);
        $this->setOrders($orders);

    }

    protected function _prepareLayout()
    {
        // $block = $this->getLayout()->createBlock(
        //     'LiberoNet_Portfolio_Block_Display',
        //     'portfolio',
        //     array('template' => 'activecodeline/developer.phtml')
        //     );
        // $this->getLayout()->getBlock('content')->append($block);
        $this->getOrders()->load();
        $this->getPorts()->load();
        // var_dump($this->getPorts()->toArray());
        $orders = $this->getOrders();
        foreach($orders as $key=>$order){
        	$flag = false;
        	foreach ($order->getAllItems() as $item) {
        		if($this->isStorage($item->getProduct()->getId())){
        			$flag = true;
        			break;
        		}
        	}
        	if(!$flag) $orders->removeItemByKey($key);
        }
        $this->setOrders($orders);
        return $this;
    }

    public function getBackUrl()
    {
        return $this->getUrl('customer/account/');
    }

//	check if a product is Storage Fee(SKU 358-360)
    public function isStorage($id){
    	return $id>=358&&$id<=360;
    }

//	check if a product is Wine according to id
    public function isWine($id){
    	return $id>373||$id<358;
    }
}