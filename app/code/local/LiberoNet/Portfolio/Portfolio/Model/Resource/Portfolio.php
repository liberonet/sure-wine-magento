<?php
class LiberoNet_Portfolio_Model_Resource_Portfolio extends Mage_Sales_Model_Resource_Order
{
     public function _construct()
     {
         $this->_init('portfolio/portfolio', 'id_libero_portfolio');
     }

     protected function _getDefaultAttributes()
    {
        return array(
            'id_libero_portfolio',
            'porduct_id',
            'customer_id',
            'created_at',
            'qty',
            'status'
        );
    }
}