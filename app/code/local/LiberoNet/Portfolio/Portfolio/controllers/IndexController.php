<?php
	class LiberoNet_Portfolio_IndexController extends Mage_Core_Controller_Front_Action
	{
		public function preDispatch()
    	{
	        parent::preDispatch();
	        $action = $this->getRequest()->getActionName();
	        $loginUrl = Mage::helper('customer')->getLoginUrl();

	        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
	            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
	        }
    	}

	   public function indexAction ()
	   {
	   	 $block = $this->getLayout()->createBlock(
            'LiberoNet_Portfolio_Block_Display',
            'portfolio',
            array('template' => 'portfolio/display.phtml')
            );
	   	 $this->getLayout()->addOutputBlock("portfolio");
	     $this->renderLayout();
	   }
	}