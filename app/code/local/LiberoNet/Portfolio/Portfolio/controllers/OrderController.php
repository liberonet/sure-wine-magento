<?php
require_once 'Mage/Sales/controllers/OrderController.php';
class LiberoNet_Portfolio_OrderController extends Mage_Sales_OrderController
{
    # Overloaded historyAction
    public function historyAction() {
    	$block = $this->getLayout()->createBlock(
            'LiberoNet_Portfolio_Block_Sales_Order',
            'history',
            array('template' => 'sales/order/history.phtml')
            );
	   	 $this->getLayout()->addOutputBlock("history");
	     $this->renderLayout();
    }
}