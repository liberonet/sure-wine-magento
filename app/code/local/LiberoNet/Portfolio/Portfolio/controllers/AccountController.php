<?php
require_once 'Mage/Customer/controllers/AccountController.php';
class LiberoNet_Portfolio_AccountController extends Mage_Customer_AccountController
{
    

    # Overloaded editAction
    public function editAction()
    {
        // $this->loadLayout();

        $this->_initLayoutMessages('customer/session');
        $this->_initLayoutMessages('catalog/session');

        $block = $this->getLayout()->createBlock(
            'Mage_Customer_Block_Form_Edit',
            'customer_edit',
            array('template' => 'customer/form/edit.phtml')
            );
        if ($block) {
            $block->setRefererUrl($this->_getRefererUrl());
        }
        $data = $this->_getSession()->getCustomerFormData(true);
        $customer = $this->_getSession()->getCustomer();
        if (!empty($data)) {
            $customer->addData($data);
        }
        if ($this->getRequest()->getParam('changepass') == 1) {
            $customer->setChangePassword(1);
        }

        // $this->getLayout()->getBlock('head')->setTitle($this->__('Account Information'));
        $this->getLayout()->getBlock('messages')->setEscapeMessageFlag(true);
        $this->getLayout()->addOutputBlock("customer_edit");
         $this->renderLayout();
    }
}