<?php
require_once 'Mage/Review/controllers/CustomerController.php';
class LiberoNet_Portfolio_ReviewController extends Mage_Review_CustomerController
{
    # Overloaded indexAction
    public function indexAction()
    {
        // $this->loadLayout();
        $this->_initLayoutMessages('catalog/session');

        // if ($navigationBlock = $this->getLayout()->getBlock('customer_account_navigation')) {
        //     $navigationBlock->setActive('review/customer');
        // }
        // if ($block = $this->getLayout()->getBlock('review_customer_list')) {
        //     $block->setRefererUrl($this->_getRefererUrl());
        // }

        // $this->getLayout()->getBlock('head')->setTitle($this->__('My Product Reviews'));
        $block = $this->getLayout()->createBlock(
            'Mage_Review_Block_Customer_List',
            'review',
            array('template' => 'review/customer/list.phtml')
            );
        $block->setTemplate('review/customer/list.phtml');
        $this->getLayout()->addOutputBlock("review");
        $this->renderLayout();
    }
}