<?php

class LiberoNet_Portfolio_Adminhtml_Portfolio_IndexController extends Mage_Adminhtml_Controller_Action
{

    protected function _construct()
    {
        $this->setUsedModuleName('LiberoNet_Portfolio');
    }

    protected $_publicActions = array('view', 'index','sell','cancelSell','edit','add','grid','invoice');

	protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/order')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));
        return $this;
    }

   public function indexAction ()
   {
   	 $this->loadLayout();

	 $this->getLayout()->getBlock('head')->setTitle($this->__('Customer Portfolio'));
     $this->renderLayout();
   }


   public function invoiceAction ()
   {
     $this->loadLayout();

     $this->getLayout()->getBlock('head')->setTitle($this->__('Invoice Wine'));
     $this->renderLayout();
   }

    public function sellAction(){
        
    	$id = $this->getRequest()->getParam('id');
	   	 $portfolio = Mage::getModel('portfolio/portfolio')->load($id);
	   	 $status = 0;
	   	if($id && Mage::getSingleton('admin/session')->isLoggedIn()){
	   	 	if(Mage::getModel('portfolio/portfolio')->sell(intval($id))){
	   	 		$price = Mage::getModel('catalog/product')->load($portfolio->getProductId())->getPrice();
	   	 		$status = 1;}
        	}
        	if($status == 1)
        		Mage::getSingleton('core/session')->addSuccess('Success'); 
            else
                Mage::getSingleton('core/session')->addError('Action Failed');
            $this->_redirectReferer();
            
    }

    public function editAction(){
        $status = 0;
        $message = '';
        $id = intval($this->getRequest()->getParam('id'));
        if(null !== $this->getRequest()->getParam('action')){
            $price = floatval($this->getRequest()->getParam('price'));
            $commision = floatval($this->getRequest()->getParam('commision'));
            $qty = intval($this->getRequest()->getParam('qty'));
            $date = $this->getRequest()->getParam('date');
            $credit_id = $this->getRequest()->getParam('credit_id');
            $stage = $this->getRequest()->getParam('stage');
            if((($shipping_from=$this->getRequest()->getParam('shipping_from'))=='other')&&$this->getRequest()->getParam('shipping_from_comment'))
                $shipping_from = $this->getRequest()->getParam('shipping_from_comment');
            if($this->getRequest()->getParam('action')=="sell"&&$price&&$qty){
                $ret = Mage::getModel('portfolio/portfolio')->sell($id,$price,$qty,$date);
                $status = $ret['status'];
                $message = $ret['message'];
                }
            else if($this->getRequest()->getParam('action')=="complete_sell"){
                $ret = Mage::getModel('portfolio/portfolio')->completeSell($id,$date,$credit_id,$shipping_from,$commision);
                $status = $ret['status'];
                $message = $ret['message'];
                }
            else if($this->getRequest()->getParam('action')=="delete"&&$qty){
                if(Mage::getModel('portfolio/portfolio')->adminDelete($id,$qty)){
                    $status = 1;}
            }else if($this->getRequest()->getParam('action')=="cancel_selling"&&$qty){
                $ret = Mage::getModel('portfolio/portfolio')->cancelSell($id,$qty);
                $status = $ret['status'];
                $message = $ret['message'];
            }else if($this->getRequest()->getParam('action')=="shipping_from"&&$shipping_from){
                $port = Mage::getModel('portfolio/portfolio')->load($id);
                if($port->getCreditId()){
                    $credit = Mage::getModel('credit/credit')->load($port->getCreditId());
                    $credit->setShippingFrom($shipping_from);
                    $credit->save();
                    $status = 1;
                }
            }else if($this->getRequest()->getParam('action')=='change_stage'&&$stage){
                    $ret = Mage::getModel('portfolio/portfolio')->changeStage($id,$stage);
                    $status = $ret['status'];
                    $message = $ret['message'];
                }
                if($status == 1)
                    Mage::getSingleton('core/session')->addSuccess('Success'); 
                echo json_encode(array('status'=>$status,'message'=>$message));
        }else{
             $port = Mage::getModel('portfolio/portfolio')->load($id);
             $product = Mage::getModel('catalog/product')->load($port->getProductId());
             $price = Mage::helper('portfolio')->getBiddingPrice($product->getPrice());
             $block = $this->getLayout()->createBlock(
            'LiberoNet_Portfolio_Block_Edit',
            'portfolio',
            array('id' => $id,'price'=>$price,'date'=>Varien_Date::now(),'qty'=>$port->getQty(),'status'=>$port->getStatus(),'stage'=>$port->getStage())
            );
         $this->getLayout()->addOutputBlock("portfolio");
         $this->renderLayout();
        }
    }

    public function addAction(){
        $status = 0;
        $customer_id = intval($this->getRequest()->getParam('customer_id'));
        $product_id = intval($this->getRequest()->getParam('product_id'));
        $order_id = intval($this->getRequest()->getParam('order_id'));
        $qty = intval($this->getRequest()->getParam('qty'));
        if(null !== $order_id&&null !== $this->getRequest()->getParam('status')&&null !== $product_id){
            $item_id = 0;
            if($order_id && $order = Mage::getModel('sales/order')->loadByIncrementId($order_id)){
                foreach($order->getItemsCollection() as $item){
                    if($item->getProductId()==$product_id)
                    {
                        $item_id = $item->getId();
                        break;
                    }
                }
            }
            if(!$item_id){
                echo json_encode(array('status'=>$status));
                exit;
            }
            if($this->getRequest()->getParam('status') == 'port'&&$product_id&&$qty){
                
                $port = Mage::getModel('portfolio/portfolio');
                $port->setCustomerId($customer_id);
                $port->setProductId($product_id);
                $port->setQty($qty);
                $port->setStatus('portfolio');
                $port->setItemId($item_id);
                $port->setCreatedAt($this->getRequest()->getParam('date'));
                $port->save();
                $status = 1;
            }else if($this->getRequest()->getParam('status') == 'sold'&&$product_id&&$qty){
                $rma_reference = intval($this->getRequest()->getParam('credit_id'));
                if($rma_reference){
                    $order = Mage::getModel('sales/order')->loadByIncrementId($order_id);
                    if($order->getId()){
                        $receiving_price = $this->getRequest()->getParam('price');
                        $port = Mage::getModel('portfolio/portfolio');
                        $port->setCustomerId($customer_id);
                        $port->setProductId($product_id);
                        $port->setQty($qty);
                        $port->setStatus('sold');
                        $port->setRmaReference($rma_reference);
                        $port->setSoldAt($credit->getCreatedAt());
                        $port->setReceivingPrice($receiving_price);
                        $port->setItemId($item_id);
                        $port->setCreatedAt($this->getRequest()->getParam('date'));
                        $port->save();
                        $status = 1;
                    }
                }
            }
            if($status == 1)
                    Mage::getSingleton('core/session')->addSuccess('Success');
            echo json_encode(array('status'=>$status));
        }else{
            $block = $this->getLayout()->createBlock(
            'LiberoNet_Portfolio_Block_Add',
            'portfolio',
            array('id' => $customer_id)
            );
         $this->getLayout()->addOutputBlock("portfolio");
         $this->renderLayout();
        }
    }

    public function editShippingFromAction(){
         $id = intval($this->getRequest()->getParam('id'));
         $portfolio = Mage::getModel('portfolio/portfolio')->load($id);
         $status = 0;
        if($id && Mage::getSingleton('admin/session')->isLoggedIn()){
            $portfolio->setShippingFrom($this->getRequest()->getParam('shipping_from'));
            $status = 1;
        }
        if($status == 1)
            Mage::getSingleton('core/session')->addSuccess('Success'); 
        echo json_encode(array('status'=>$status));

    }
    public function deleteAction(){
            $id = $this->getRequest()->getParam('id');
             $portfolio = Mage::getModel('portfolio/portfolio')->load($id);
             $status = 0;
            if($id && Mage::getSingleton('admin/session')->isLoggedIn()){
                if(Mage::getModel('portfolio/portfolio')->deletePort($id)){
                    //$price = Mage::getModel('catalog/product')->load($portfolio->getProductId())->getPrice();
                    $status = 1;}
            }
            if($status == 1)
                Mage::getSingleton('core/session')->addSuccess('Success'); 
            else
                Mage::getSingleton('core/session')->addError('Action Failed');
            $this->_redirectReferer();
    }

    public function cancelSellAction(){	
   		$id = $this->getRequest()->getParam('id');
	   	 $portfolio = Mage::getModel('portfolio/portfolio')->load($id);
	   	 $status = 0;
	   	if($id && Mage::getSingleton('admin/session')->isLoggedIn()){
	   	 	
	   	 	if(Mage::getModel('portfolio/portfolio')->cancelSell($id))
	   	 		$status = 1;
		    }
	    
        	if($status == 1)
        		Mage::getSingleton('core/session')->addSuccess('Success'); 
        	else

        		Mage::getSingleton('core/session')->addError('Failed'); 
            $this->_redirectReferer();

		}

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Export wine selling grid to CSV format
     */
    public function exportCsvAction()
    {
        $customerId = (int) $this->getRequest()->getParam('id');
        $customer = Mage::getModel('customer/customer');
        $fileName   = 'Portfolio Selling';
        if ($customerId) {
            $customer->load($customerId);
            Mage::register('current_customer', $customer);
            $fileName .= ' '.$customer->getName();
        }
        $fileName .= '.csv';
        $grid       = $this->getLayout()->createBlock('portfolio/adminhtml_portfolio_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
    public function winesellingAction(){
        $customerId = (int) $this->getRequest()->getParam('id');
        $customer = Mage::getModel('customer/customer');
        if ($customerId) {
            $customer->load($customerId);
            Mage::register('current_customer', $customer);
        }

        $this->loadLayout();
        $this->renderLayout();
    }
}