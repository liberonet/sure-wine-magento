<?php

class LiberoNet_Portfolio_Adminhtml_Portfolio_InvoiceController extends Mage_Adminhtml_Controller_Action
{

    protected function _construct()
    {
        $this->setUsedModuleName('LiberoNet_Portfolio');
    }

    protected $_publicActions = array( 'index','grid','invoice');

	protected function _initAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('sales/order')
            ->_addBreadcrumb($this->__('Sales'), $this->__('Sales'))
            ->_addBreadcrumb($this->__('Orders'), $this->__('Orders'));
        return $this;
    }

   public function indexAction ()
   {
   	 $this->loadLayout();

	 $this->getLayout()->getBlock('head')->setTitle($this->__('Customer Portfolio'));
     $this->renderLayout();
   }


   public function wineAction ()
   {
        $this->loadLayout();
        $this->renderLayout();
   }

    public function gridAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    /**
     * Export wine selling grid to CSV format
     */
    public function exportCsvAction()
    {
        $customerId = (int) $this->getRequest()->getParam('id');
        $customer = Mage::getModel('customer/customer');
        $fileName   = 'Wine Invoice Report';
        $fileName .= '.csv';
        $grid       = $this->getLayout()->createBlock('portfolio/adminhtml_invoice_grid');
        $this->_prepareDownloadResponse($fileName, $grid->getCsvFile());
    }
}