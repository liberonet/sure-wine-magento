<?php
	class LiberoNet_Portfolio_IndexController extends Mage_Core_Controller_Front_Action
	{
		public function preDispatch()
    	{
	        parent::preDispatch();
	        $action = $this->getRequest()->getActionName();
	        $loginUrl = Mage::helper('customer')->getLoginUrl();

	        if (!Mage::getSingleton('customer/session')->authenticate($this, $loginUrl)) {
	            $this->setFlag('', self::FLAG_NO_DISPATCH, true);
	        }
    	}

	   public function indexAction ()
	   {
	   	 $this->loadLayout();

    	 $this->getLayout()->getBlock('head')->setTitle($this->__('My Portfolio'));
	     $this->renderLayout();
	   }

	   public function displayAction ()
	   {
	   	 $block = $this->getLayout()->createBlock(
            'LiberoNet_Portfolio_Block_Display',
            'portfolio',
            array('template' => 'portfolio/display.phtml')
            );
	   	 $this->getLayout()->addOutputBlock("portfolio");
	     $this->renderLayout();
	   }
	   public function winesoldAction ()
	   {
	   	 $block = $this->getLayout()->createBlock(
            'LiberoNet_Portfolio_Block_Winesold',
            'winesold',
            array('template' => 'portfolio/winesold.phtml')
            );
	   	 $this->getLayout()->addOutputBlock("winesold");
	     $this->renderLayout();
	   }

	   public function sellAction ()
	   {
	   	 $id = $this->getRequest()->getParam('id');
	   	 $portfolio = Mage::getModel('portfolio/portfolio')->load($id);
	   	 $isAjax = Mage::app()->getRequest()->isAjax();
	   	 $status = 0;
	   	if($id && $isAjax
	   		&& Mage::getSingleton('customer/session')->isLoggedIn() 
	   		&& Mage::getSingleton('customer/session')->getCustomerId()==$portfolio->getCustomerId()){
	   	 	if(Mage::getModel('portfolio/portfolio')->sell($id)){
	   	 		$price = Mage::getModel('catalog/product')->load($portfolio->getProductId())->getPrice();
	   	 		$status = 1;}
        	}
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('price' => $price,'status'=>$status)));
	   }

	   public function cancelSellAction(){	
   		$id = $this->getRequest()->getParam('id');
	   	 $portfolio = Mage::getModel('portfolio/portfolio')->load($id);
	   	 $isAjax = Mage::app()->getRequest()->isAjax();
	   	 $status = 0;
	   	if($id && $isAjax
	   		&& Mage::getSingleton('customer/session')->isLoggedIn() 
	   		&& Mage::getSingleton('customer/session')->getCustomerId()==$portfolio->getCustomerId()){
	   	 	
	   	 	if(Mage::getModel('portfolio/portfolio')->cancelSell($id))
	   	 		$status = 1;
		    }
	    $this->getResponse()->setBody(Mage::helper('core')->jsonEncode(array('status' =>$status )));

		}
}