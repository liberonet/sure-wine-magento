<?php

/**
 * Export purchase order to csv format
 *
 */
class MDN_Purchase_Model_Order_Csv  extends Mage_Core_Model_Abstract
{
    private $_order;
    private $_endLine = "\r\n";

    /**
     * Set purchase order
     * @param <type> $order
     * @return MDN_Purchase_Model_Order_Csv
     */
    public function setOrder($order)
    {
        $this->_order = $order;
        return $this;
    }

    /**
     * Return file name
     * @return <type>
     */
    public function getFileName()
    {
        return mage::helper('purchase')->__('Purchase Order #%s from %s', $this->_order->getpo_order_id(), $this->_order->getSupplier()->getsup_name()).'.csv';
    }

    /**
     * return content type
     */
    public function getContentType()
    {
        return 'text/csv';
    }

    /**
     * Return csv content
     */
    public function getCsv()
    {
        $template = 'ContactName,EmailAddress,POAddressLine1,POAddressLine2,POAddressLine3,POAddressLine4,POCity,PORegion,POPostalCode,POCountry,InvoiceNumber,Reference,InvoiceDate,DueDate,PlannedDate,Total,TaxTotal,InvoiceAmountPaid,InvoiceAmountDue,InventoryItemCode,Description,Quantity,UnitAmount,Discount,LineAmount,AccountCode,TaxType,TaxAmount,TrackingName1,TrackingOption1,TrackingName2,TrackingOption2,Currency,Type,Sent,Status'.$this->_endLine;
        $supplier = $this->_order->getSupplier();
        
        $csv = $template;
        
        foreach($this->_order->getProducts() as $product)
        {
            $line = $template;

            $line = str_replace('ContactName', $supplier->sup_name, $line);
            $line = str_replace('EmailAddress', $supplier->sup_email, $line);
            $line = str_replace('POAddressLine1', $supplier->sup_address1, $line);
            $line = str_replace('POAddressLine2', $supplier->sup_address2, $line);
            $line = str_replace('POAddressLine3', '', $line);
            $line = str_replace('POAddressLine4', '', $line);
            $line = str_replace('POCity', $supplier->sup_city, $line);
            $line = str_replace('PORegion', '', $line);
            $line = str_replace('POPostalCode', $supplier->sup_zipcode, $line);
            $line = str_replace('POCountry', $supplier->sup_country, $line);
            $line = str_replace('InvoiceNumber', $this->_order->po_order_id, $line);
            $line = str_replace('Reference', '', $line);
            $line = str_replace('InvoiceDate', $this->_order->po_date, $line);
            $line = str_replace('DueDate', $this->_order->po_supply_date, $line);
            $line = str_replace('PlannedDate', '', $line);
            $line = str_replace('Total', $product->getRowTotal(), $line);
            $line = str_replace('TaxTotal', '0', $line);
            $line = str_replace('InvoiceAmountPaid', $this->_order->po_paid, $line);
            $line = str_replace('InvoiceAmountDue', $product->getRowTotal() - $this->_order->po_paid, $line);
            $line = str_replace('InventoryItemCode', $product->getsku(), $line);
            $line = str_replace('Description', $product->getpop_product_name(), $line);
            $line = str_replace('Quantity', $product->getpop_qty(), $line);
            $line = str_replace('UnitAmount', $product->getpop_price_ht(), $line);
            $line = str_replace('Discount', $product->getpop_discount(), $line);
            $line = str_replace('LineAmount', $product->getRowTotal(), $line);
            $line = str_replace('AccountCode', '610-000', $line);
            $line = str_replace('TaxType', 'Tax Exempt', $line);
            $line = str_replace('TaxAmount', '0', $line);
            $line = str_replace('TrackingName1', '', $line);
            $line = str_replace('TrackingOption1', '', $line);
            $line = str_replace('TrackingName2', '', $line);
            $line = str_replace('TrackingOption2', '', $line);
            $line = str_replace('Currency', $supplier->sup_currency, $line);
            $line = str_replace('Type', 'Bill', $line);
            $line = str_replace('Sent', '', $line);
            $line = str_replace('Status', 'Awaiting', $line);
            $csv .= $line;
        }

        return $csv;
    }

}