<?php

/**
 * Data Helper
 *
 * @category   Webinse
 * @package    Webinse_Crm
 * @author     Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Helper_Data extends Mage_Core_Helper_Abstract{

    public function isSalesOrderViewPage(){
        if(Mage::app()->getRequest()->getControllerName() == 'sales_order' ||
            strpos(Mage::helper('core/url')->getCurrentUrl(), 'sales_order') !== false
        ){

            return true;
        }

        return false;
    }

    /**
     * Prepare reminder's statuses.
     *
     * @return array
     */
    public function getAvailableStatuses(){
        $statuses = array();
        foreach(Mage::getResourceModel('crm/statuses_collection')->getItems() as $item){
            $statuses[$item->getCode()] = $item->getTitle();
        }

        return $statuses;
    }

    /**
     * Prepare reminder's groups.
     *
     * @return array
     */
    public function getAvailableGroups(){
        $groups = array();
        foreach(Mage::getResourceModel('crm/groups_collection')->getItems() as $item){
            $groups[$item->getId()] = $item->getGroupName();
        }

        return $groups;
    }

    /**
     * Check if reminder is ended
     *
     * @param object | timestamp $reminder
     * @return bool
     */
    public function isReminderEnded($dueDate){
        if(is_object($dueDate)){
            return ($dueDate->getDuedatetime() && strtotime('+1 day', strtotime($dueDate->getDuedatetime())) < Mage::getModel('core/date')->timestamp());
        } else{
            return ($dueDate && (strtotime('+1 day', $dueDate) < Mage::getModel('core/date')->timestamp()));
        }
    }

    /**
     * Retrieve reminder status by using start and due dates
     *
     * @param timestamp $startDate
     * @param timestamp $dueDate
     * @return string
     */
    public function getStatusByDate($startDate, $dueDate = null){
        $currentTime = Mage::getModel('core/date')->timestamp();

        $status = 'pending';
        $isReminderEnded = $this->isReminderEnded($dueDate);
        if($startDate <= $currentTime && !$isReminderEnded){
            $status = 'running';
        } elseif($isReminderEnded){
            $status = 'failed';
        }

        return $status;
    }

    /**
     * Retrieve all users by group ID
     *
     * @param $groupId
     * @return array
     */
    public function getUsersIdByGroupId($groupId){
        $arrayUsersId = array();
        $groupCollection = Mage::getResourceModel('crm/relations_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('group_id', array('eq' => $groupId));

        foreach($groupCollection as $usersId){
            $arrayUsersId[] = $usersId->getUserId();
        }
        return $arrayUsersId;
    }

}
