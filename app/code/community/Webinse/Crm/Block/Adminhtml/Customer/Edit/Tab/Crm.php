<?php
/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 10/16/13
 */
class Webinse_Crm_Block_Adminhtml_Customer_Edit_Tab_Crm
    extends Mage_Adminhtml_Block_Template
    implements Mage_Adminhtml_Block_Widget_Tab_Interface
{

    public function getTabLabel()
    {
        return Mage::helper('sales')->__('CRM');
    }

    public function getTabTitle()
    {
        return Mage::helper('sales')->__('CRM');
    }

    public function canShowTab()
    {
        return true;
    }

    public function isHidden()
    {
        return false;
    }
}
