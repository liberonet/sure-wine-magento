<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Widget_Grid_Column_Renderer_Statuses extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract {

    public function render(Varien_Object $row) {
        $value = $row->getData($this->getColumn()->getIndex());

        if ($value === 'Pending') {
            $value = '<p style="margin-bottom: 0;text-align: center;text-transform: uppercase;background-color: #66CCFF">' . $value . '</p>';
        } elseif ($value === 'Running') {
            $value = '<p style="margin-bottom: 0;text-align: center;text-transform: uppercase;background-color: #66FFCC">' . $value . '</p>';
        } elseif ($value === 'Completed') {
            $value = '<p style="margin-bottom: 0;text-align: center;text-transform: uppercase;background-color: #FFCC66">' . $value . '</p>';
        } elseif ($value === 'Failed') {
            $value = '<p style="margin-bottom: 0;text-align: center;text-transform: uppercase;background-color: #ff8579">' . $value . '</p>';
        }

        return $value;
    }

}
?>

