<?php

/**
 * Attachment Grid Block
 *
 * @category   Webinse
 * @package    Webinse_Crm
 * @author     Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Attachment_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('crm_attachment_grid');
        $this->setDefaultSort('attachment_id');
        $this->setDefaultDir('DESC');
        $this->setFilterVisibility(false);
        $this->setPagerVisibility(false);
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel('crm/attachments_collection');

        if ($this->getRequest()->getRequestedControllerName() == 'customer') {
            $collection->addFieldToFilter('customer_id', array('eq' => $this->getRequest()->getParam('id', false)));
        }
        else {
            $collection->addFieldToFilter('order_id', array('eq' => $this->getRequest()->getParam('order_id', false)));
        }

        $this->setCollection($collection);
        parent::_prepareCollection();
        return $this;
    }

    protected function _prepareColumns()
    {
        $helper   = Mage::helper('core');

        $this->addColumn('attachment_id', array(
            'header' => $helper->__('#'),
            'index'  => 'attachment_id',
            'width'  => '20px',
            'type'   => 'number'
        ));

        $this->addColumn('file_name', array(
            'header' => $helper->__('File Name'),
            'width'  => '150px',
            'type'   => 'text',
            'index'  => 'file_name'
        ));
        $this->addColumn('user_name', array(
            'header' => $helper->__('User Name'),
            'width'  => '150px',
            'type'   => 'text',
            'index'  => 'user_name'
        ));
        $this->addColumn('user_email', array(
            'header' => $helper->__('User Email'),
            'width'  => '150px',
            'type'   => 'text',
            'index'  => 'user_email'
        ));

        $this->addColumn('download', array(
            'width'    => '25px',
            'type'     => 'action',
            'getter'   => 'getId',
            'actions'  => array(
                array(
                    'caption' => Mage::helper('catalog')->__('Download'),
                    'url'     => array('base' => 'crm/attachment/download'),
                    'field'   => 'id'
                )
            ),
            'filter'   => false,
            'sortable' => false
        ));

        $this->addColumn('remove', array(
            'width'    => '25px',
            'type'     => 'action',
            'getter'   => 'getId',
            'actions'  => array(
                array(
                    'caption' => Mage::helper('catalog')->__('Remove'),
                    'url'     => array('base' => 'crm/attachment/remove'),
                    'field'   => 'id'
                )
            ),
            'filter'   => false,
            'sortable' => false
        ));

        return parent::_prepareColumns();
    }

}