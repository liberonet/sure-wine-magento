<?php

/**
 * Attachments Block
 *
 * @category   Webinse
 * @package    Webinse_Crm
 * @author     Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Attachment extends Mage_Adminhtml_Block_Abstract
{

    /**
     * Retrieve available instance
     *
     * @return Mage_Customer_Model_Customer | Mage_Sales_Model_Order
     */
    protected function _getInstance()
    {
        $keys = array('current_order', 'order', 'current_customer', 'customer');
        foreach ($keys as $key) {
            if (Mage::registry($key)) {
                return Mage::registry($key);
            }
        }

        Mage::throwException(Mage::helper('sales')->__('Cannot get object instance to output attachments.'));
    }

    /**
     * Generate upload action url
     *
     * @return varchar
     */
    public function generateUploadUrl()
    {
        $param = array();

        $instance = $this->_getInstance();

        if ($instance->getQuoteId()) {
            $param = array('order_id' => $instance->getId());
        }
        else {
            $param = array('customer_id' => $instance->getId());
        }

        return Mage_Adminhtml_Helper_Data::getUrl('crm/attachment/upload', $param);
    }

}