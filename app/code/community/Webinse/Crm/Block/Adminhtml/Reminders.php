<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/22/13
 */
class Webinse_Crm_Block_Adminhtml_Reminders extends Mage_Adminhtml_Block_Abstract
{

    private function _setReferer()
    {

        if (Mage::helper('crm')->isSalesOrderViewPage()) {
            return 'sales';
        }
        else {
            return 'customer';
        }
    }

    private function _sendId()
    {
        ($this->_setReferer() == 'sales') ? $_id = 'order_id' : $_id = 'id';

        $key = ($_id == 'id') ? 'customer_id' : 'order_id';
        return array('key' => $key, 'id'  => Mage::app()->getRequest()->getParam($_id));
    }

    protected function _prepareLayout()
    {
        $sendId = $this->_sendId();
        $this->setChild('add_button', $this->getLayout()->createBlock('adminhtml/widget_button')
                        ->setData(array(
                            'label'    => Mage::helper('catalog')->__('Add New Reminder'),
                            'class'    => 'add',
                            'id'       => 'add_new_reminder',
                            'on_click' => "setLocation('" . Mage_Adminhtml_Helper_Data::getUrl('crm/grid/new', array($sendId['key'] => $sendId['id'], 'referer'      => $this->_setReferer())) . "');"
                        ))
        );

        return parent::_prepareLayout();
    }

    public function getAddButtonHtml()
    {
        return $this->getChildHtml('add_button');
    }

}