<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/27/13
 */
class Webinse_Crm_Block_Adminhtml_Dashboard_Reminders extends Mage_Adminhtml_Block_Abstract{

    /**
     * Saving in $_data
     * @return mixed
     */
    public function loadReminders(){
        if(!$this->hasData('reminders')){
            $this->setData('reminders', $this->_initReminders());
        }
        return $this->getData('reminders');
    }

    public function getCustomerName($user_id){
        return Mage::getModel('admin/user')->load($user_id)->getName();
    }

    public function generateCustomerName($id, $customer = false){
        if($customer){
            $string = Mage::getModel('customer/customer')->load($customer)->getName() . ' (Customer ID <a href="' . Mage::helper('adminhtml')->getUrl("*/customer/edit", array('id' => $customer)) . '">#' . $customer . '</a>)';
        } else{
            $order = Mage::getModel('sales/order')->load($id);
            $string = $order->getCustomerName() . ' (Order ID <a href="' . Mage::helper('adminhtml')->getUrl("*/sales_order/view", array('order_id' => $order->getId())) . '">#' . $order->getRealOrderId() . '</a>)';
        }

        return $string;
    }

    public function generateUrl($id){
        return Mage::helper('adminhtml')->getUrl('crm/grid/edit', array('id' => (int)$id));
    }

    /**
     * Load collection
     * @return bool | array
     */
    private function _initReminders(){
        //firstly update statuses for all reminders
        Mage::getModel('crm/reminders')->updateStatuses();

        //then prepare collection with new statuses
        $col = Mage::getResourceModel('crm/reminders_collection')
            ->addFieldToFilter('done', array('eq' => false))
            ->addFieldToFilter('datetime', array('to' => Mage::getModel('core/date')->gmtDate('Y-m-d', '+0 day')));

        $currentUserId = Mage::getSingleton('admin/session')->getUser()->getId();
        if($currentUserId){
            //get group that assign to customer
            $arrayGroups = Mage::getModel('crm/groups')->getGroupsByUserId($currentUserId);
            if(count($arrayGroups) > 0){
                $col->addFieldToFilter(array('user_id', 'group_id'),
                    array(
                        array('eq' => $currentUserId),
                        array($arrayGroups)
                    ));
            }
            else{
                $col->addFieldToFilter('user_id', array('eq' => $currentUserId));
            }
        }

        return ($col->getSize()) ? $col->getItems() : false;
    }

    public function getReminder(){
        if($id = (int)$this->getRequest()->getParam('id')){
            $reminder = Mage::getModel('crm/reminders')->load($id);
            if(!$reminder->getId() && $id){
                return $this->__('This reminder no longer exists.');
            }

            return $reminder;
        } else{
            return $this->__('Please, select reminder.');
        }
    }

}
