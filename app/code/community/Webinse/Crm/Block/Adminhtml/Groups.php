<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Groups extends Mage_Adminhtml_Block_Widget_Grid_Container{
    protected function _construct(){
        $this->_addButtonLabel = Mage::helper('crm')->__('Add New Group');
        $this->_blockGroup = 'crm';
        $this->_controller = 'adminhtml_groups';
        $this->_headerText = Mage::helper('crm')->__('Groups');
    }
}