<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/23/13
 */
class Webinse_Crm_Block_Adminhtml_Reminder_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    public function __construct()
    {
        $this->_objectId   = 'id';
        $this->_controller = 'adminhtml_reminder';
        $this->_blockGroup = 'crm';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('cms')->__('Save Reminder'));
        $this->_updateButton('delete', 'label', Mage::helper('cms')->__('Delete Reminder'));
    }

    public function getSaveUrl()
    {
        return Mage_Adminhtml_Helper_Data::getUrl('crm/grid/save');
    }

    public function getBackUrl()
    {
        return Mage::helper('core/http')->getHttpReferer();
    }

    public function getHeaderText()
    {
        $crmReminder = Mage::registry('crm_reminder');
        if ($crmReminder->getId()) {
            if ($crmReminder->getOrderId()) {
                return Mage::helper('core')->__("Edit Reminder for Order #%s", $this->htmlEscape($crmReminder->getOrderId()));
            }
            else {
                return Mage::helper('core')->__("Edit Reminder for Customer #%s", $this->htmlEscape($crmReminder->getCustomerId()));
            }
        }
        else {
            return Mage::helper('core')->__('New Reminder');
        }
    }

}
