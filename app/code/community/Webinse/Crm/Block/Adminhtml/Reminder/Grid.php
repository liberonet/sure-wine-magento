<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/22/13
 */
class Webinse_Crm_Block_Adminhtml_Reminder_Grid extends Mage_Adminhtml_Block_Widget_Grid{

    public function __construct(){
        parent::__construct();
        $this->setId('crm_grid');
        //$this->setUseAjax(true);
        $this->setDefaultSort('reminder_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);

        if(Mage::app()->getRequest()->getControllerName() != 'grid'){
            $this->setFilterVisibility(false);
            $this->setPagerVisibility(false);
        } else{
            Mage::getSingleton('adminhtml/session')->addNotice(Mage::helper('adminhtml')->__('To create reminder, please go to Sales Order View page or Customer Edit page'));
        }

        //update statuses for all reminders
        Mage::getModel('crm/reminders')->updateStatuses();
    }

    protected function _prepareCollection(){
        $collection = Mage::getResourceModel('crm/reminders_collection');
        $coreResource = Mage::getSingleton('core/resource');

        if($this->getRequest()->getRequestedControllerName() == 'customer'){
            $collection->addFieldToFilter('customer_id', array('eq' => $this->getRequest()->getParam('id', false)));
        } elseif($this->getRequest()->getRequestedControllerName() == 'sales_order'){
            $collection->addFieldToFilter('order_id', array('eq' => $this->getRequest()->getParam('order_id', false)));
        }
        if($this->getRequest()->getControllerName() !== 'grid'){
            $collection->addFieldToFilter('done', 0);
        }

        $collection->getSelect()->joinLeft($coreResource->getTableName('crm/statuses'), 'main_table.status = ' . $coreResource->getTableName('crm/statuses') . '.code', array(
            'status_title' => 'title',
        ));

        $collection->getSelect()->joinLeft($coreResource->getTableName('crm/relations'), 'main_table.group_id = ' . $coreResource->getTableName('crm/relations') . '.group_id', array(
            'group' => 'group_id',
        ));

        $collection->getSelect()->from(
            $coreResource->getTableName('admin/user'), array('name' => new Zend_Db_Expr("CONCAT(firstname, ' ', lastname)"),
        ))
            ->where($coreResource->getTableName('admin/user') . '.user_id = main_table.user_id');

        $collection->getSelect()->group('main_table.reminder_id');
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){
        $helper = Mage::helper('core');
        $coreResource = Mage::getSingleton('core/resource');

        $this->addColumn('reminder_id', array(
            'header' => $helper->__('#'),
            'index' => 'reminder_id',
            'width' => '20px',
            'type' => 'number'
        ));


        $this->addColumn('name', array(
            'header' => $helper->__('Order assigned to'),
            'width' => '150px',
            'type' => 'text',
            'index' => 'name',
            'filter_index' => 'concat_ws(" ", ' . $coreResource->getTableName('admin/user') . '.firstname, ' . $coreResource->getTableName('admin/user') . '.lastname)'
        ));

        $this->addColumn('group', array(
            'header' => $helper->__('Order assigned to group'),
            'width' => '150px',
            'type' => 'options',
            'index' => 'group',
            'filter_index' => $coreResource->getTableName('crm/relations') . '.group_id',
            'options' => Mage::helper('crm')->getAvailableGroups(),
            'width' => '200px',
        ));

        $this->addColumn('datetime', array(
            'header' => $helper->__('Start Date'),
            'width' => '150px',
            'type' => 'date',
            'index' => 'datetime'
        ));

        $this->addColumn('duedatetime', array(
            'header' => $helper->__('Due Date'),
            'width' => '150px',
            'type' => 'date',
            'index' => 'duedatetime'
        ));

        $this->addColumn('title', array(
            'header' => $helper->__('Note'),
            'type' => 'text',
            'index' => 'title',
            'filter_index' => 'main_table.title'
        ));

        $this->addColumn('status_title', array(
            'header' => $helper->__('Status'),
            'type' => 'options',
            'index' => 'status_title',
            'filter_index' => $coreResource->getTableName('crm/statuses') . '.title',
            'options' => Mage::helper('crm')->getAvailableStatuses(),
            'renderer' => 'Webinse_Crm_Block_Adminhtml_Widget_Grid_Column_Renderer_Statuses',
            'width' => '200px',
        ));

        $this->addColumn('action', array(
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('catalog')->__('Edit'),
                    'url' => array('base' => 'crm/grid/edit'),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false
        ));

        $this->addColumn('complete', array(
            'width' => '50px',
            'type' => 'action',
            'getter' => 'getId',
            'actions' => array(
                array(
                    'caption' => Mage::helper('catalog')->__('Complete'),
                    'url' => array('base' => 'crm/grid/complete/order_id/' . $this->getRequest()->getParam('order_id')),
                    'field' => 'id'
                )
            ),
            'filter' => false,
            'sortable' => false
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($row){
        return $this->getUrl('crm/grid/edit', array('id' => $row->getData('reminder_id')));
    }

}
