<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/22/13
 */
class Webinse_Crm_Block_Adminhtml_Reminder_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('block_form');
        $this->setTitle(Mage::helper('cms')->__('Block Information'));
    }

    /**
     * Load Wysiwyg on demand and Prepare layout
     */
    protected function _prepareLayout()
    {
        parent::_prepareLayout();
        if (Mage::getSingleton('cms/wysiwyg_config')->isEnabled()) {
            $this->getLayout()->getBlock('head')->setCanLoadTinyMce(true);
        }
    }

    private function _check_orders($customer_id)
    {
        $orders = Mage::getModel('sales/order')
                ->getCollection()
                ->addFieldToSelect('increment_id')
                ->addFieldToFilter('customer_id', array('eq' => $customer_id));
        $orders->getSelect()->limit(1);
        return !$orders->getSize() > 0;
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('crm_reminder');

        $form = new Varien_Data_Form(array('id'     => 'edit_form', 'action' => $this->getData('action'), 'method' => 'post'));

        $form->setHtmlIdPrefix('reminder_');

        $fieldset = $form->addFieldset('base_fieldset', array('legend' => Mage::helper('core')->__('Reminder Information'), 'class'  => 'fieldset-wide'));

        if ($model->getId()) {
            $fieldset->addField('reminder_id', 'hidden', array(
                'name' => 'reminder_id',
            ));
        }

        if (Mage::app()->getRequest()->getActionName() == 'edit' && $model->getData('customer') == 1 && $this->_check_orders($model->getData('order_id'))) {
            $fieldset->addField('new', 'note', array(
                'name' => 'new',
                'text' => Mage::helper('cms')->__('Customer hasn\'t placed an orders yet'),
            ));
        }

        $fieldset->addField('user_id', 'select', array(
            'name'     => 'user_id',
            'label'    => Mage::helper('core')->__('Task assigned to'),
            'title'    => Mage::helper('core')->__('Task assigned to'),
            'values'   => Mage::getModel('crm/source_users')->toOptionArray(),
            'required' => true
        ));

        $fieldset->addField('group_id', 'select', array(
            'name'     => 'group_id',
            'label'    => Mage::helper('core')->__('Task assigned to group'),
            'title'    => Mage::helper('core')->__('Task assigned to group'),
            'values'   => Mage::getModel('crm/source_groups')->toOptionArray(),
            'required' => false
        ));

        $fieldset->addField('done', 'select', array(
            'name'     => 'done',
            'label'    => Mage::helper('cms')->__('Completed'),
            'required' => true,
            'values'   => Mage::getSingleton('adminhtml/system_config_source_yesno')->toOptionArray()
        ));

        $dateFormatIso = Mage::app()->getLocale()->getDateFormat(
            Mage_Core_Model_Locale::FORMAT_TYPE_SHORT
        );
        
        $fieldset->addField('datetime', 'date', array(
            'name'     => 'datetime',
            'label'    => Mage::helper('core')->__('Reminder Date'),
            'title'    => Mage::helper('core')->__('Reminder Date'),
            'format'    => $dateFormatIso,
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'required' => true
        ));

        $fieldset->addField('duedatetime', 'date', array(
            'name'     => 'duedatetime',
            'label'    => Mage::helper('core')->__('Due Date'),
            'title'    => Mage::helper('core')->__('Due Date'),
            'format'    => $dateFormatIso,
            'image'    => $this->getSkinUrl('images/grid-cal.gif'),
            'note'     => 'If not used please leave empty.',
            'required' => false
        ));

        $fieldset->addField('title', 'text', array(
            'name'     => 'title',
            'label'    => Mage::helper('core')->__('Note'),
            'title'    => Mage::helper('core')->__('Note'),
            'required' => true
        ));

        $urlHelper     = Mage::getSingleton('adminhtml/url');
        $wysiwygConfig = Mage::getSingleton('cms/wysiwyg_config')->getConfig();
        $wysiwygConfig->addData(array(
            'add_variables'            => false,
            'plugins'                  => array(),
            'widget_window_url'        => $urlHelper->getUrl('adminhtml/widget/index'),
            'directives_url'           => $urlHelper->getUrl('adminhtml/cms_wysiwyg/directive'),
            'directives_url_quoted'    => preg_quote($urlHelper->getUrl('adminhtml/cms_wysiwyg/directive')),
            'files_browser_window_url' => $urlHelper->getUrl('adminhtml/cms_wysiwyg_images/index'),
        ));

        $fieldset->addField('content', 'editor', array(
            'name'    => 'content',
            'label'   => Mage::helper('core')->__('Content'),
            'title'   => Mage::helper('core')->__('Content'),
            'style'   => 'height:36em',
            'config'  => $wysiwygConfig,
            'wysiwyg' => true,
        ));

        $form->setValues($model->getData());

        //set logged in user for default
        if (Mage::getSingleton('admin/session')->getUser()->getId() && !$model->getId()) {
            $form->getElement('user_id')->setValue(Mage::getSingleton('admin/session')->getUser()->getId());
        }

        if (Mage::registry('order_id') || $model->getOrderId()) {
            $fieldset->addField('order_id', 'hidden', array(
                'name'  => 'order_id',
                'label' => Mage::helper('core')->__('Related to Order ID'),
                'title' => Mage::helper('core')->__('Related to Order ID'),
            ));

            $form->getElement('order_id')->setValue((Mage::registry('order_id')) ? Mage::registry('order_id') : $model->getOrderId());
        }
        if (Mage::registry('customer_id') || $model->getCustomerId()) {
            $fieldset->addField('customer_id', 'hidden', array(
                'name'  => 'customer_id',
                'label' => Mage::helper('core')->__('Related to Customer ID'),
                'title' => Mage::helper('core')->__('Related to Customer ID'),
            ));

            $form->getElement('customer_id')->setValue((Mage::registry('customer_id')) ? Mage::registry('customer_id') : $model->getCustomerId());
        }

        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }

}
