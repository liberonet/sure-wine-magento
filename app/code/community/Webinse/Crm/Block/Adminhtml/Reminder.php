<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/22/13
 */
class Webinse_Crm_Block_Adminhtml_Reminder extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected $_blockGroup = 'crm';

    public function __construct()
    {
        $this->_controller     = 'adminhtml_reminder';
        $this->_headerText     = Mage::helper('crm')->__('CRM Reminders');
        $this->_addButtonLabel = Mage::helper('crm')->__('Add New Reminder');
        parent::__construct();
    }

    /**
     * Redefine header css class
     *
     * @return string
     */
    public function getHeaderCssClass()
    {
        return 'icon-head head-crm';
    }

}