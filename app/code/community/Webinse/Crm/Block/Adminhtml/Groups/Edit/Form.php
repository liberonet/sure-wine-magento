<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Groups_Edit_Form extends Mage_Adminhtml_Block_Widget_Form{
    protected function _prepareForm(){
        $group = Mage::registry('current_group');
        $form = new Varien_Data_Form();
        $fieldset = $form->addFieldset('edit_group', array(
            'legend' => Mage::helper('crm')->__('Group Info')
        ));

        if($group->getId()){
            $fieldset->addField('group_id', 'hidden', array(
                'name' => 'group_id',
                'required' => true
            ));
        }

        $this->setTemplate('webinse/crm/groups/form.phtml');

        $fieldset->addField('group_name', 'text', array(
            'name' => 'group_name',
            'label' => Mage::helper('crm')->__('Group Name'),
            'maxlength' => '250',
            'required' => true,
        ));
        $fieldset->addField('in_group_user', 'hidden',
            array(
                'name' => 'in_group_user',
                'id' => 'in_group_user',
            )
        );
        $fieldset->addField('in_group_user_old', 'hidden',
            array(
                'name' => 'in_group_user_old',
                'id' => 'in_group_user_old',
            )
        );


        $form->setMethod('post');
        $form->setUseContainer(true);
        $form->setId('edit_form');
        $form->setAction($this->getUrl('*/*/saveGroup'));
        $form->setValues($group->getData());

        $this->setForm($form);

        $this->setChild('userGrid', $this->getLayout()->createBlock('crm/adminhtml_groups_user_grid', 'groupUsersGrid'));

    }

    protected function _getGridHtml(){
        return $this->getChildHtml('userGrid');
    }
}