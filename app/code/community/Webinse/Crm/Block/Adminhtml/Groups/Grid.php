<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Groups_Grid extends Mage_Adminhtml_Block_Widget_Grid{
    protected function _construct(){
        $this->setId('groupsGrid');
        $this->_controller = 'groups';
        $this->setUseAjax(true);

        $this->setDefaultSort('group_id');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('crm/groups')->getCollection();
        $this->setCollection($collection);

        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){
        $this->addColumn('group_id', array(
            'header' => Mage::helper('crm')->__('ID'),
            'align' => 'right',
            'width' => '20px',
            'filter_index' => 'group_id',
            'index' => 'group_id'
        ));

        $this->addColumn('group_name', array(
            'header' => Mage::helper('crm')->__('Group Name'),
            'align' => 'left',
            'filter_index' => 'group_name',
            'index' => 'group_name',
            'type' => 'text',
            'truncate' => 50,
            'escape' => true,
        ));

        return parent::_prepareColumns();
    }

    public function getRowUrl($group){
        return $this->getUrl('*/*/editgroup', array(
            'group_id' => $group->getId(),
        ));
    }

    public function getGridUrl(){
        return $this->getUrl('*/*/grid', array('_current' => true));
    }
}