<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Groups_Edit extends Mage_Adminhtml_Block_Widget_Form_Container{
    protected function _construct(){
        $this->_objectId = 'group_id';
        $this->_blockGroup = 'crm';
        $this->_mode = 'edit';
        $this->_controller = 'adminhtml_groups';

        $group_id = (int)$this->getRequest()->getParam('group_id');
        $group = Mage::getModel('crm/groups')->load($group_id);
        Mage::register('current_group', $group);

        $this->updateButton('save', 'label', Mage::helper('crm')->__('Save Group'));
        $this->updateButton('delete', 'label', Mage::helper('crm')->__('Delete Group'));
    }

    public function getHeaderText(){
        $group = Mage::registry('current_group');
        if($group->getId()){
            return Mage::helper('crm')->__("Edit Group '%s'", $this->escapeHtml($group->getGroupName()));
        } else{
            return Mage::helper('crm')->__("Add New Group");
        }
    }
}