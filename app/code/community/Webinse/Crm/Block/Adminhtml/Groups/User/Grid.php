<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Webinse Team <info@webinse.com>
 */
class Webinse_Crm_Block_Adminhtml_Groups_User_Grid extends Mage_Adminhtml_Block_Widget_Grid{

    public function __construct(){
        parent::__construct();
        $this->setDefaultSort('group_user_id');
        $this->setDefaultDir('asc');
        $this->setId('groupUserGrid');
        $this->setDefaultFilter(array('in_group_users' => 1));
        $this->setUseAjax(true);
    }

    protected function _prepareCollection(){
        $collection = Mage::getModel('admin/user')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns(){
        $this->addColumn('in_group_users', array(
            'header_css_class' => 'a-center',
            'type' => 'checkbox',
            'name' => 'in_group_users',
            'align' => 'center',
            'values' => $this->_getUsers(),
            'index' => 'user_id',
            'field_name' => 'group_id[]',

        ));

        $this->addColumn('role_user_id', array(
            'header' => Mage::helper('crm')->__('User ID'),
            'width' => 5,
            'align' => 'left',
            'sortable' => true,
            'index' => 'user_id'
        ));

        $this->addColumn('group_user_username', array(
            'header' => Mage::helper('crm')->__('User Name'),
            'align' => 'left',
            'index' => 'username'
        ));

        $this->addColumn('group_user_firstname', array(
            'header' => Mage::helper('crm')->__('First Name'),
            'align' => 'left',
            'index' => 'firstname'
        ));

        $this->addColumn('group_user_lastname', array(
            'header' => Mage::helper('crm')->__('Last Name'),
            'align' => 'left',
            'index' => 'lastname'
        ));

        $this->addColumn('group_user_email', array(
            'header' => Mage::helper('crm')->__('Email'),
            'width' => 40,
            'align' => 'left',
            'index' => 'email'
        ));

        $this->addColumn('group_user_is_active', array(
            'header' => Mage::helper('crm')->__('Status'),
            'index' => 'is_active',
            'align' => 'left',
            'type' => 'options',
            'options' => array('1' => Mage::helper('crm')->__('Active'), '0' => Mage::helper('crm')->__('Inactive')),
        ));

        return parent::_prepareColumns();
    }

    public function getGridUrl(){
        $groupId = $this->getRequest()->getParam('group_id');
        return $this->getUrl('*/*/editgroupgrid', array('group_id' => $groupId));
    }

    protected function _getUsers($json = false){
        $groupId = $this->getRequest()->getParam('group_id');

        $collection = Mage::getResourceModel('crm/relations_collection')
            ->addFieldToSelect(array('group_id', 'user_id'))
            ->addFieldToFilter('group_id', array("eq" => $groupId));

        $usersArray = array();
        foreach($collection as $user){
            $usersArray[] = $user->getUserId();
        }
        if(count($usersArray) > 0){
            if($this->getRequest()->getParam('in_group_user') != ''){
                return $this->getRequest()->getParam('in_group_user');
            }
            if($json){
                $jsonUsers = Array();
                foreach($usersArray as $usrid){
                    $jsonUsers[$usrid] = 0;
                }
                return Mage::helper('core')->jsonEncode((object)$jsonUsers);
            } else{
                return array_values($usersArray);
            }
        }
    }
}
