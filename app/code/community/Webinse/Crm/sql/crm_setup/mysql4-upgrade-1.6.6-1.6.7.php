<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
        ->newTable($installer->getTable('crm/statuses'))
        ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
                ), 'Status ID')
        ->addColumn('code', Varien_Db_Ddl_Table::TYPE_VARCHAR, '20')
        ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, '40')
        ->setComment(Mage::helper('core')->__('Table with reminders statuses for Webinse_Crm extension'));

$installer->getConnection()->createTable($table);

$statuses = array(
    'pending'   => 'Pending',
    'running'   => 'Running',
    'completed' => 'Completed'
);
foreach ($statuses as $key => $value) {
    Mage::getModel('crm/statuses')
            ->setCode($key)
            ->setTitle($value)
            ->save();
}

$installer->endSetup();
