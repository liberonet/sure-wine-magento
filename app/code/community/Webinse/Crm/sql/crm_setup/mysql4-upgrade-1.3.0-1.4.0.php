<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$installer->getConnection()->modifyColumn(
        $installer->getTable('crm/reminders'),
        'datetime', array(
            'type' => Varien_Db_Ddl_Table::TYPE_DATE,
            'comment' => 'Date for Reminder')
        );

$installer->endSetup();