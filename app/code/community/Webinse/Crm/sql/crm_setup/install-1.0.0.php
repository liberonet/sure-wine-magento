<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
        ->newTable($installer->getTable('crm/attachments'))
        ->addColumn('attachment_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
                ), 'Attachment ID')
        ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER)
        ->addColumn('path', Varien_Db_Ddl_Table::TYPE_TEXT)
        ->setComment(Mage::helper('core')->__('Table with attachments info for Webinse_Crm extension'));

$installer->getConnection()->createTable($table);
$installer->endSetup();