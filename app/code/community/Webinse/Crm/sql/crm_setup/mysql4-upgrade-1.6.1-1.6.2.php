<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

$connection->addColumn($installer->getTable('crm/attachments'), 'file_name', 'VARCHAR (255) NULL AFTER `path`');

$installer->endSetup();