<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

$connection->addColumn($installer->getTable('crm/reminders'), 'status', 'VARCHAR (20) NULL');

$helper = Mage::helper('crm');
$reminders = Mage::getResourceModel('crm/reminders_collection')->getItems();
foreach ($reminders as $reminder) {
    if ($reminder->getDone()){
        $reminder->setStatus('completed');
    }
    else{
        $reminder->setStatus($helper->getStatusByDate(strtotime($reminder->getDatetime())));
    }
    
    $reminder->save();
}

$installer->endSetup();
