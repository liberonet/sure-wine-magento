<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

$connection->addColumn($installer->getTable('crm/reminders'), 'user_firstname', 'VARCHAR (255) NULL AFTER `user_id`');
$connection->addColumn($installer->getTable('crm/reminders'), 'user_lastname', 'VARCHAR (255) NULL AFTER `user_firstname`');

$installer->endSetup();