<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

$connection->addColumn($installer->getTable('crm/attachments'), 'customer_id', array(
    'nullable' => false,
    'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
    'default'  => false,
    'comment'  => "Customer_id")
);

$connection->addColumn($installer->getTable('crm/attachments'), 'user_name', 'VARCHAR (255) NULL');
$connection->addColumn($installer->getTable('crm/attachments'), 'user_email', 'VARCHAR (255) NULL');

$installer->endSetup();