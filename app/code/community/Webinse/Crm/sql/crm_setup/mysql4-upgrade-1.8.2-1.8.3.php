<?php
$installer = $this;

$installer->startSetup();

$installer->getConnection()
    ->addColumn($installer->getTable('crm/reminders'), 'group_id', array(
            'nullable' => true,
            'type'     => Varien_Db_Ddl_Table::TYPE_INTEGER,
            'default'  => null,
            'comment'  => "group_id",
            'after'=> 'user_id'
        ));

$installer->endSetup();