<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
        ->newTable($installer->getTable('crm/reminders'))
        ->addColumn('reminder_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
            'identity' => true,
            'nullable' => false,
            'primary'  => true,
                ), 'Reminder ID')
        ->addColumn('customer_id', Varien_Db_Ddl_Table::TYPE_INTEGER)
        ->addColumn('order_id', Varien_Db_Ddl_Table::TYPE_INTEGER)
        ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER)
        ->addColumn('note', Varien_Db_Ddl_Table::TYPE_TEXT)
        ->addColumn('datetime', Varien_Db_Ddl_Table::TYPE_DATETIME)
        
        ->addIndex($installer->getIdxName('crm/reminders', array('order_id')), array('order_id'))
        ->addForeignKey($installer->getFkName('crm/reminders', 'order_id', 'sales/order', 'entity_id'),
                'order_id',
                $installer->getTable('sales/order'), 'entity_id',
                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

        ->addIndex($installer->getIdxName('crm/reminders', array('customer_id')), array('customer_id'))
        ->addForeignKey($installer->getFkName('crm/reminders', 'customer_id', 'customer/entity', 'entity_id'),
                'customer_id',
                $installer->getTable('customer/entity'), 'entity_id',
                Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)

        ->setComment(Mage::helper('core')->__('Table with reminders info for Webinse_Crm extension'));

$installer->getConnection()->createTable($table);
$installer->endSetup();