<?php
$installer = $this;

$installer->startSetup();

//table crm_groups
$tableGroup = $installer->getConnection()
    ->newTable($installer->getTable('crm/groups'))
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Group ID')
    ->addColumn('group_name', Varien_Db_Ddl_Table::TYPE_TEXT)
    ->setComment(Mage::helper('core')->__('Table with groups for Webinse_Crm extension'));

//table crm_relations
$tableRelations = $installer->getConnection()
    ->newTable($installer->getTable('crm/relations'))
    ->addColumn('entity_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'nullable' => false,
        'primary'  => true,
    ), 'Entity ID')
    ->addColumn('group_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addColumn('user_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'nullable' => false,
    ))
    ->addForeignKey(
        $installer->getFkName(
            'crm/relations',
            'group_id',
            'crm/groups',
            'group_id'
        ),
        'group_id', $installer->getTable('crm/groups'),
        'group_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->addForeignKey(
        $installer->getFkName(
            'crm/relations',
            'user_id',
            'admin/user',
            'user_id'
        ),
        'user_id', $installer->getTable('admin/user'),
        'user_id',
        Varien_Db_Ddl_Table::ACTION_CASCADE, Varien_Db_Ddl_Table::ACTION_CASCADE)
    ->setComment(Mage::helper('core')->__('Table with relations of groups and users for Webinse_Crm extension'));


$installer->getConnection()->createTable($tableGroup);
$installer->getConnection()->createTable($tableRelations);
$installer->endSetup();