<?php
$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()->addColumn($installer->getTable('crm/reminders'), 'duedatetime', Varien_Db_Ddl_Table::TYPE_DATETIME);

$installer->endSetup();