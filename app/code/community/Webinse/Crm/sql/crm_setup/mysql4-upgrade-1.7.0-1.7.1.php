<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();
$statuses = array(
    'failed'   => 'Failed',
);
foreach ($statuses as $key => $value) {
    Mage::getModel('crm/statuses')
        ->setCode($key)
        ->setTitle($value)
        ->save();
}
$installer->endSetup();
