<?php

/**
 * @var $installer Mage_Core_Model_Resource_Setup
 */
$installer = $this;
$installer->startSetup();

$connection = $installer->getConnection();

$connection->changeColumn($installer->getTable('crm/reminders'), 'note', 'content', 'TEXT NOT NULL');
$connection->addColumn($installer->getTable('crm/reminders'), 'title', 'VARCHAR (255) NULL AFTER `user_id`');

$installer->endSetup();