<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/17/13
 */
class Webinse_Crm_AjaxController extends Mage_Adminhtml_Controller_Action
{

    public function completeAction()
    {
        if ($id = (int)$this->getRequest()->getParam('id')) {
            $model = Mage::getModel('crm/reminders')->load($id);
            if (!$model->getId() && $id) {
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This reminder no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            if (!$model->getDone()) {
                if ($model->getOrderId()) {
                    $order = Mage::getModel('sales/order')->load($model->getOrderId());
                    $order->addStatusToHistory($order->getStatus(), 'The reminder has been complete(' . $model->getTitle() . ')', false);
                    $order->save();
                }

                $model->setTitle($model->getTitle() . ' (Task has been complete)');
                $model->setDone(1);
                $model->save();
                $completedStatus = 'completed';
                $this->_sendTransactional($id, $completedStatus);
            }
        }

        return;
    }

    public function viewReminderAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    /**
     * Send email notification
     *
     * @param $reminder
     * @param $newStatus
     */
    protected function _sendTransactional($reminder, $newStatus)
    {
        $model = Mage::getModel('crm/reminders');
        try {
            //if assigned on group
            if ($reminder->getGroupId()) {
                $model->sendTransactional($reminder, $newStatus);
            }
        } catch (Exception $e) {
            Mage::getSingleton('admin/session')->addError($e->getMessage());
        }
    }

}

