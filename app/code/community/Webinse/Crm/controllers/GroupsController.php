<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Webinse Team <info@webinse.com>
 */
class Webinse_Crm_GroupsController extends Mage_Adminhtml_Controller_Action{
    /**
     * Preparing layout for output
     *
     * @return Mage_Adminhtml_Permissions_RoleController
     */
    protected function _initAction(){
        $this->loadLayout();
        $this->_setActiveMenu('system/acl');
        $this->_addBreadcrumb($this->__('System'), $this->__('System'));
        $this->_addBreadcrumb($this->__('Permissions'), $this->__('Permissions'));
        $this->_addBreadcrumb($this->__('Groups'), $this->__('Groups'));
        return $this;
    }

    /**
     * Initialize role model by passed parameter in request
     *
     * @return Mage_Admin_Model_Roles
     */
    protected function _initGroup($requestVariable = 'group_id'){
        $this->_title($this->__('System'))
            ->_title($this->__('Permissions'))
            ->_title($this->__('Groups'));

        if(!Mage::registry('current_group')){
            $group = Mage::getModel('crm/groups')->load($this->getRequest()->getParam($requestVariable));
            Mage::register('current_group', $group);
        }

        return Mage::registry('current_group');
    }

    /**
     * Show grid with roles existing in systems
     *
     */
    public function indexAction(){
        $this->_title($this->__('System'))
            ->_title($this->__('Permissions'))
            ->_title($this->__('Groups'));

        $this->_initAction();

        $this->renderLayout();
    }

    /**
     * Edit role action
     *
     */
    public function editGroupAction(){
        $this->_initAction();
        $group = $this->_initGroup();

        if($group->getId()){
            $breadCrumb = $this->__('Edit Group');
            $breadCrumbTitle = $this->__('Edit Group');
        } else{
            $breadCrumb = $this->__('Add New Group');
            $breadCrumbTitle = $this->__('Add New Group');
        }

        $this->_title($group->getId() ? $group->getGroupName() : $this->__('New Group'));

        $this->_addBreadcrumb($breadCrumb, $breadCrumbTitle);

        $this->_addJs(
            $this->getLayout()->createBlock('adminhtml/template')->setTemplate('webinse/crm/groups/users_grid_js.phtml')
        );

        $this->renderLayout();
    }

    /**
     * Remove group action
     *
     */
    public function deleteAction(){
        try{
            $group = $this->_initGroup()->delete();

            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The role has been deleted.'));
        } catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while deleting this role.'));
        }

        $this->_redirect("*/*/");
    }

    /**
     * Group form submit action to save or create new group
     *
     */
    public function saveGroupAction(){
        $gid = $this->getRequest()->getParam('group_id', false);
        $groupUsers = $this->getRequest()->getParam('in_group_user', null);
        parse_str($groupUsers, $groupUsers);
        $groupUsers = array_keys($groupUsers);

        $oldGroupUsers = $this->getRequest()->getParam('in_group_user_old');
        parse_str($oldGroupUsers, $oldGroupUsers);
        $oldGroupUsers = array_keys($oldGroupUsers);

        $group = $this->_initGroup('group_id');
        if(!$group->getId() && $gid){
            Mage::getSingleton('adminhtml/session')->addError($this->__('This Group no longer exists.'));
            $this->_redirect('*/*/');
            return;
        }

        try{
            $groupName = $this->getRequest()->getParam('group_name');

            $group->setGroupName($groupName);
            $group->save();

            foreach($oldGroupUsers as $oGid){
                $this->_deleteUsersFromGroup($oGid, $group->getId());
            }
            foreach($groupUsers as $nGuid){
                $this->_addUserToGroup($nGuid, $group->getId());
            }
            Mage::getSingleton('adminhtml/session')->addSuccess($this->__('The group has been successfully saved.'));
        } catch(Mage_Core_Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
        } catch(Exception $e){
            Mage::getSingleton('adminhtml/session')->addError($this->__('An error occurred while saving this group.'));
        }

        $this->_redirect('*/*/');
        return;
    }


    protected function newAction(){
        $this->_redirect('*/*/editgroup');
    }

    /**
     * Assign user to group
     *
     * @param int $userId
     * @param int $roleId
     * @return bool
     */
    protected function _addUserToGroup($userId, $groupId){
        try{
            if(!empty($groupId)){
                $relations = Mage::getResourceModel('crm/relations_collection')
                    ->addFieldToSelect(array('group_id', 'user_id'))
                    ->addFieldToFilter('group_id', array('eq' => $groupId))
                    ->addFieldToFilter('user_id', array('eq' => $userId));
                if(!$relations->getData()){
                    Mage::getModel('crm/relations')
                        ->setGroupId($groupId)
                        ->setUserId($userId)
                        ->save();
                }
            }
        } catch(Exception $e){
            throw $e;
            return false;
        }
        return true;
    }

    /**
     * remove users from group
     *
     * @param $oGid
     * @param $groupId
     * @return bool
     * @throws Exception
     */
    protected function _deleteUsersFromGroup($oGid, $groupId){
        try{
            if(!empty($groupId)){
                $relations = Mage::getResourceModel('crm/relations_collection')
                    ->addFieldToSelect(array('group_id', 'user_id'))
                    ->addFieldToFilter('group_id', array('eq' => $groupId))
                    ->addFieldToFilter('user_id', array('eq' => $oGid));
                if($relations->getData()){
                    foreach($relations as $relation){
                        Mage::getModel('crm/relations')->load($relation->getEntityId())
                            ->delete()
                            ->save();
                    }
                }
            }
        } catch(Exception $e){
            throw $e;
            return false;
        }
        return true;
    }

    /**
     * Action for ajax request from assigned users grid
     *
     */
    public function editgroupgridAction(){
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('crm/adminhtml_groups_user_grid')->toHtml()
        );
    }


} 
