<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/22/13
 */
class Webinse_Crm_GridController extends Mage_Adminhtml_Controller_Action{

    /**
     * Init actions
     *
     * @return Mage_Adminhtml_Cms_PageController
     */
    protected function _initAction(){
        // load layout, set active menu and breadcrumbs
        $this->loadLayout()
            ->_setActiveMenu('crm')
            ->_addBreadcrumb(Mage::helper('core')->__('CRM'), Mage::helper('core')->__('CRM'))
            ->_title($this->__('CRM'));
        return $this;
    }

    protected function _sendTransactional($reminder){
        if($reminder->getGroupId()){
            Mage::getModel('crm/reminders')->sendTransactional($reminder, $reminder->getStatus());
        }
    }

    public function indexAction(){
        $this->_initAction();
        $this->_title($this->__('Reminders'));
        //$this->_addContent($this->getLayout()->createBlock('crm/adminhtml_reminder_grid'));
        $this->renderLayout();
    }

    public function editAction(){
        // 1. Get ID and create model
        $id = (int)$this->getRequest()->getParam('id');
        $model = Mage::getModel('crm/reminders');

        // 2. Initial checking
        if($id){
            $model->load($id);
            if(!$model->getId()){
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('core')->__('This reminder no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
        }

        $data = Mage::getSingleton('adminhtml/session')->getFormData(true);
        if(!empty($data)){
            $model->setData($data);
        }

        Mage::register('crm_reminder', $model);

        $this->_initAction();
        $this->_title($this->__('Reminder'))
            ->_title($model->getId() ? $model->getTitle() : $this->__('New Reminder'));
        $this->renderLayout();
    }

    public function completeAction(){
        if($id = (int)$this->getRequest()->getParam('id')){
            $model = Mage::getModel('crm/reminders')->load($id);
            if(!$model->getId() && $id){
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This reminder no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }
            try{
                if(!$model->getDone()){
                    if($model->getOrderId()){
                        $order = Mage::getModel('sales/order')->load($model->getOrderId());
                        $order->addStatusToHistory($order->getStatus(), 'The reminder has been complete(' . $model->getTitle() . ')', false);
                        $order->save();
                    }

                    $model->setTitle($model->getTitle() . ' (Task has been complete)');
                    $model->setDone(1);
                    $model->setStatus('completed');

                    // save the data
                    $model->save();

                    $this->_sendTransactional($model);
                }
                // display success message
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);
            } catch(Exception $e){
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
        $this->getResponse()->setRedirect($this->_getRefererUrl());
    }

    public function saveAction(){
        // check if data sent
        if($data = $this->getRequest()->getPost()){
            $data = $this->_filterPostData($data);

            //init model and set data
            $model = Mage::getModel('crm/reminders');

            if($id = (int)$this->getRequest()->getParam('reminder_id')){
                $model->load($id);
            }

            if(!$model->getId() && $id){
                Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('This reminder no longer exists.'));
                $this->_redirect('*/*/');
                return;
            }

            // init model and set data
            $model->addData($data);
            if(isset($data['done']) && $data['done']){
                $model->setStatus('completed');
            } else{
                $model->setStatus(Mage::helper('crm')->getStatusByDate(strtotime($data['datetime']), strtotime($data['duedatetime'])));
            }

            $user = Mage::getModel('admin/user')->load((int)$data['user_id']);
            if($user->getId()){
                $model->setUserFirstname($user->getFirstname());
                $model->setUserLastname($user->getLastname());
            }

            // try to save it
            try{
                // save the data
                $model->save();
                //send email notification, if reminder assigned on group
                if($data['group_id']){
                    if($id){
                        Mage::getModel('crm/reminders')
                            ->sendTransactional($model, 'edited');
                    }else{
                        Mage::getModel('crm/reminders')
                            ->sendTransactional($model, 'create');
                    }

                }
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cms')->__('The reminder has been saved.'));
                // clear previously saved data from session
                Mage::getSingleton('adminhtml/session')->setFormData(false);

                // check if 'Save and Continue'
                if($this->getRequest()->getParam('back')){
                    $this->_redirect('*/*/edit', array('id' => $model->getId()));
                    return;
                }

                if($this->getRequest()->getParam('order_id') || $model->getOrderId()){
                    // go to order view page
                    $this->_redirect('adminhtml/sales_order/view', array('order_id' => (int)($model->getOrderId()) ? $model->getOrderId() : $this->getRequest()->getParam('order_id')));
                } elseif($this->getRequest()->getParam('customer_id') || $model->getCustomerId()){
                    // go to customer view page
                    $this->_redirect('adminhtml/customer/edit', array('id' => (int)($model->getCustomerId()) ? $model->getCustomerId() : $this->getRequest()->getParam('order_id')));
                }

                return;
            } catch(Exception $e){
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // save data in session
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                // redirect to edit form
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('reminder_id')));
                return;
            }
        }

        $this->_redirect('*/*/');
    }

    /**
     * Filtering posted data. Converting localized data if needed
     *
     * @param array
     * @return array
     */
    protected function _filterPostData($data){
        return $this->_filterDates($data, array('datetime', 'duedatetime'));
    }

    public function newAction(){
        // the same form is used to create and edit
        if($this->getRequest()->getParam('order_id')){
            Mage::register('order_id', (int)$this->getRequest()->getParam('order_id'));
        } elseif($this->getRequest()->getParam('customer_id')){
            Mage::register('customer_id', (int)$this->getRequest()->getParam('customer_id'));
        }

        $this->_forward('edit');
    }

    public function deleteAction(){
        // check if we know what should be deleted
        if($id = (int)$this->getRequest()->getParam('id')){
            try{
                // init model and delete
                $model = Mage::getModel('crm/reminders');
                $model->load($id);
                $model->delete();
                // display success message
                Mage::getSingleton('adminhtml/session')->addSuccess(Mage::helper('cms')->__('The reminder has been deleted.'));
                // go to grid
                $this->_redirect('*/*/');
                return;
            } catch(Exception $e){
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                // go back to edit form
                $this->_redirect('*/*/edit', array('id' => $id));
                return;
            }
        }
        // display error message
        Mage::getSingleton('adminhtml/session')->addError(Mage::helper('cms')->__('Unable to find a reminder to delete.'));
        // go to grid
        $this->_redirect('*/*/');
    }

}

