<?php

/**
 * Attachment Controller
 *
 * @category   Webinse
 * @package    Webinse_Crm
 * @author     Webinse Team <info@webinse.com>
 */
class Webinse_Crm_AttachmentController extends Mage_Adminhtml_Controller_Action
{

    protected function _getUser()
    {
        return Mage::getSingleton('admin/session')->getUser();
    }

    /**
     * Download attachment
     */
    public function downloadAction()
    {
        $attachment = Mage::getModel('crm/attachments')->load((int) $this->getRequest()->getParam('id'));

        if ($attachment->getId()) {
            $filePath = Mage::getBaseDir('media') . '/' . $attachment->getPath();

            if (is_file($filePath) && is_readable($filePath)) {
                $this->getResponse()
                        ->setHttpResponseCode(200)
                        ->setHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0', true)
                        ->setHeader('Pragma', 'public', true)
                        ->setHeader('Content-type', 'application/force-download')
                        ->setHeader('Content-Length', filesize($filePath))
                        ->setHeader('Content-Disposition', 'attachment' . '; filename=' . basename($filePath));
                $this->getResponse()->clearBody();
                $this->getResponse()->sendHeaders();
                readfile($filePath);
            }
        }

        return;
    }

    /**
     * Save file to media folder and assign it to customer or order
     */
    public function uploadAction()
    {
        $id = 0;
        if ($id = $this->getRequest()->getParam('order_id')) {
            $field = 'order_id';
        }
        elseif ($id    = $this->getRequest()->getParam('customer_id')) {
            $field = 'customer_id';
        }

        if ($id && isset($_FILES['attachment_file']['name']) && (file_exists($_FILES['attachment_file']['tmp_name']))) {
            try {
                $path     = Mage::getBaseDir('media') . DS . Webinse_Crm_Model_Attachments::SAVE_PATH . DS;
                $filename = str_replace(array(' ', '*', '^', '%', '$', '&', '#'), '_', strtolower($_FILES['attachment_file']['name']));
                $uploader = new Varien_File_Uploader('attachment_file');
                $uploader->setAllowedExtensions(array('doc', 'pdf', 'txt', 'docx', 'png', 'jpeg', 'jpg'));
                $uploader->setAllowCreateFolders(true);
                $uploader->setAllowRenameFiles(false);
                $uploader->setFilesDispersion(false);
                $uploader->save($path . DS . $id, $filename);

                //save attachment info
                $user = $this->_getUser();
                Mage::getModel('crm/attachments')->addData(array(
                    $field       => $id,
                    'path'       => Webinse_Crm_Model_Attachments::SAVE_PATH . DS . $id . DS . $filename,
                    'file_name'  => $filename,
                    'user_name'  => $user->getFirstname() . ' ' . $user->getLastname(),
                    'user_email' => $user->getEmail()
                ))->save();

                $this->_getSession()->addSuccess($this->__('Attachment has been added successfully.'));
                $this->_redirectReferer();
            }
            catch (Exception $e) {
                echo 'Error Message: ' . $e->getMessage();
            }
        }
        else {
            $this->_getSession()->addError($this->__('Please, specify attachment for upload.'));
            $this->_redirectReferer();
        }
    }

    /**
     * Remove attachment from media
     */
    public function removeAction()
    {
        $attachment = Mage::getModel('crm/attachments')->load((int) $this->getRequest()->getParam('id'));

        if ($attachment->getId()) {
            unlink(Mage::getBaseDir('media') . '/' . $attachment->getPath());
            $attachment->delete();

            $this->_getSession()->addSuccess($this->__('Attachment has been removed successfully.'));
        }
        else {
            $this->_getSession()->addError($this->__('Please, specify attachment for remove.'));
        }

        $this->_redirectReferer();
    }

}
