<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Groups extends Mage_Core_Model_Abstract{

    protected function _construct(){
        $this->_init('crm/groups');
    }

    /**
     * Retrieve all groups by user ID
     *
     * @param $userId
     * @return array
     */
    public function getGroupsByUserId($userId){
        $arrayGroupsId = array();
        $collection = Mage::getResourceModel('crm/relations_collection')
            ->addFieldToSelect('*')
            ->addFieldToFilter('user_id', array("eq" => $userId));

        foreach($collection as $item){
            $arrayGroupsId[] = $item->getGroupId();
        }
        return $arrayGroupsId;

    }
} 