<?php

/**
 * @category   Webinse
 * @package    Webinse_Crm
 * @author     Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Observer
{

    public function crmCoreBlockAbstractPrepareLayoutBefore($observer)
    {
        $block = $observer->getEvent()->getBlock();
        if ($block->getNameInLayout() == 'notifications' && $block->getChild('crm_reminders_notifications')) {
            if (Mage::getStoreConfig('crm/general/pages') == 1 && strpos($block->getRequest()->getControllerName(), 'dashboard') === false) {
                $block->unsetChild('crm_reminders_notifications');
            }
        }
    }

}