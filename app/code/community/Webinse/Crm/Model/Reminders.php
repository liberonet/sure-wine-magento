<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/22/13
 */
class Webinse_Crm_Model_Reminders extends Mage_Core_Model_Abstract{

    const CONFIG_PATH = 'crm/notification/';

    protected function _construct(){
        $this->_init('crm/reminders');
    }

    /**
     * @param $reminder
     * @param $newStatus
     */
    public function sendTransactional($reminder, $newStatus){
        $storeId = Mage::app()->getStore()->getId();

        // Switch Transactional Email Template's ID
        switch($newStatus){
            case '0':
                $templateId = Mage::getStoreConfig(self::CONFIG_PATH . 'created_email_template', $storeId);
                break;
            case 'running':
                $templateId = Mage::getStoreConfig(self::CONFIG_PATH . 'running_email_template', $storeId);
                break;
            case 'failed':
                $templateId = Mage::getStoreConfig(self::CONFIG_PATH . 'fail_email_template', $storeId);
                break;
            case 'completed':
                $templateId = Mage::getStoreConfig(self::CONFIG_PATH . 'complete_email_template', $storeId);
                break;
        }

        // Set sender information
        $sender = array(
            'name' => Mage::getStoreConfig(self::CONFIG_PATH . 'sender_name', $storeId),
            'email' => Mage::getStoreConfig(self::CONFIG_PATH . 'sender_email', $storeId)
        );

        // Set recipient information
        $adminModel = Mage::getModel('admin/user')->load($reminder->getUserId());
        $recipientEmail = $adminModel->getEmail();
        $recipientName = $adminModel->getFirstname() . ' ' . $adminModel->getLastname();

        // Set variables that can be used in email template
        $vars = array(
            'title' => $reminder->getTitle(),
            'username' => $recipientName,
            'reminderUrl' => Mage::helper('adminhtml')->getUrl('crm/grid/edit', array('id' => (int)$reminder->getId())),
            'startDateTime' => $reminder->getDatetime(),
            'dueDateTime' => $reminder->getDuedatetime(),
            'oldStatus' => $reminder->getStatus(),
            'newStatus' => $newStatus
        );

        // Send Transactional Email
        if(!$reminder->getGroupId()){
            if(isset($templateId)){
                Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $recipientEmail, $recipientName, $vars, $storeId);
            }
        } else{
            // Send Transactional Email to group
            $this->sendGroupTransactional($reminder->getGroupId(), $sender, $templateId, $vars, $storeId);
        }
    }

    /**
     * Send email notification for group user
     *
     * @param $groupId
     * @param $sender
     * @param $templateId
     * @param $vars
     * @param $storeId
     */
    protected function sendGroupTransactional($groupId, $sender, $templateId, $vars, $storeId){
        $userIds = Mage::helper('crm')->getUsersIdByGroupId($groupId);
        $adminUser = Mage::getModel('admin/user');

        foreach ($userIds as $userId) {
            $user = $adminUser->load($userId);
            $recipientEmails[] = $user->getEmail();
            $recipientNames[] = $user->getFirstname() . ' ' . $user->getLastname();
        }

        if (isset($templateId)) {
            try {
                $mail = Mage::getModel('core/email_template')->sendTransactional($templateId, $sender, $recipientEmails, $recipientNames, $vars, $storeId);
                if (!$mail->getSentSuccess()) {
                    Mage::log('Email didn\'t sent to ' . var_dump($recipientEmails) . '.');
                }
            } catch (Exception $e) {
                // display error message
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            }
        }
    }

    /**
     * Update status for all reminders if that needed
     */
    public function updateStatuses(){
        $helper = Mage::helper('crm');
        $reminders = $this->_getResource()->getAllReminders();

        foreach($reminders as $reminder){
            $newStatus = ($reminder['done']) ? 'completed' : $helper->getStatusByDate(strtotime($reminder['datetime']), ((isset($reminder['duedatetime'])) ? strtotime($reminder['duedatetime']) : null));
            if($newStatus != $reminder['status']){
                $reminderObject = $this->load((int)$reminder['reminder_id']);
                if(!$reminderObject->getId()){
                    continue;
                }

                $reminderObject->setStatus($newStatus)->save();
                $this->sendTransactional($reminderObject, $newStatus);
            }
        }
    }

}