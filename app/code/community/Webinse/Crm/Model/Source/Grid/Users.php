<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 10/1/13
 */
class Webinse_Crm_Model_Source_Grid_Users{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(){
        $options = array();
        $collection = Mage::getResourceModel('admin/user_collection')->load();
        foreach($collection->getItems() as $item){
            $options[$item->getId()] = $item->getData('firstname') . ' ' . $item->getData('lastname');
        }
        return $options;

    }

}
