<?php
/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/23/13
 */

class Webinse_Crm_Model_Source_Grid_Yesno
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(1 => Mage::helper('adminhtml')->__('Yes'), 0 => Mage::helper('adminhtml')->__('No'));
    }

}
