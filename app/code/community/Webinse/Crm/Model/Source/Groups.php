<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Source_Groups{
    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(){
        $groupArray = array();

        $groupArray[] = array(
            'label' => '',
            'value' => 0
        );

        foreach(Mage::getResourceModel('crm/groups_collection')->getItems() as $item){
            array_push($groupArray, array('label' => $item->getGroupName(), 'value' => $item->getGroupId()));
        }
        return $groupArray;
    }
}