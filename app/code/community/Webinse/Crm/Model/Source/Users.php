<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 10/1/13
 */
class Webinse_Crm_Model_Source_Users{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray(){
        $options = array();
        $collection = Mage::getModel('admin/user')->getCollection()->addFieldToFilter('is_active', 1);
        foreach($collection->getItems() as $item){
            array_push($options, array('label' => $item->getData('firstname') . ' ' . $item->getData('lastname'), 'value' => $item->getId()));
        }
        return $options;

    }

}
