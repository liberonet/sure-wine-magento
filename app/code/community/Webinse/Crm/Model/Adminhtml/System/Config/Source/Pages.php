<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Adminhtml_System_Config_Source_Pages
{

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => 1, 'label' => Mage::helper('crm')->__('Dashboard')),
            array('value' => 2, 'label' => Mage::helper('crm')->__('All Pages')),
        );
    }

}
