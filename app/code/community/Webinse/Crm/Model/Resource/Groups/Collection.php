<?php
/**
 * Created by PhpStorm.
 * User: mikhail
 * Date: 7/22/14
 * Time: 5:53 PM
 */

class Webinse_Crm_Model_Resource_Groups_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{
    /**
     * Initialize resource model
     *
     */
    protected function _construct()
    {
        $this->_init('crm/groups');
    }

} 