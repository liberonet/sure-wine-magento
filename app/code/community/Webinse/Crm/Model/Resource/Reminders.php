<?php

/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/22/13
 */
class Webinse_Crm_Model_Resource_Reminders extends Mage_Core_Model_Resource_Db_Abstract
{

    protected function _construct()
    {
        $this->_init('crm/reminders', 'reminder_id');
    }

    /**
     * Retrieve all reminders
     * 
     * @return array
     */
    public function getAllReminders()
    {
        $select = $this->_getReadAdapter()
                ->select()
                ->from($this->getMainTable())
                ->reset(Zend_Db_Select::COLUMNS)
                ->columns('*');

        return $this->_getReadAdapter()->fetchAll($select);
    }

}