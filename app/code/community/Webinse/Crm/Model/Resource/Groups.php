<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Resource_Groups extends Mage_Core_Model_Resource_Db_Abstract{
    protected function _construct(){
        $this->_init('crm/groups', 'group_id');
    }
} 