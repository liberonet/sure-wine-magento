<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Resource_Relations_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract{
    /**
     * Initialize resource model
     *
     */
    protected function _construct(){
        $this->_init('crm/relations');
    }

} 