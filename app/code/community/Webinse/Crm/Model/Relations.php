<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Relations extends Mage_Core_Model_Abstract{

    protected function _construct(){
        $this->_init('crm/relations');
    }
} 