<?php

/**
 * @category    Webinse
 * @package     Webinse_Crm
 * @author      Alena Tsareva <alena.tsareva@webinse.com>
 */
class Webinse_Crm_Model_Cron{

    /**
     * Update reminder's status
     */
    public function updateStatuses(){
        Mage::getModel('crm/reminders')->updateStatuses();
    }
} 