<?php
/**
 * Created by Webinse (www.webinse.com)
 * User: Sam Petrenko (sam.petrenko@webinse.com)
 * Date: 9/21/13
 */

class Webinse_Crm_Model_Attachments extends Mage_Core_Model_Abstract
{

    const SAVE_PATH = 'crm_attachments';

    protected function _construct()
    {
        $this->_init('crm/attachments');
    }
}