<?php

class Fooman_Connect_Block_Adminhtml_Tax_Rate_Form extends Mage_Adminhtml_Block_Tax_Rate_Form
{

    protected function _prepareForm()
    {
        parent::_prepareForm();

        if (Mage::helper('foomanconnect/config')->isConfigured()) {
            $rateModel = Mage::getSingleton('tax/calculation_rate');
            $fieldset = $this->getForm()->addFieldset(
                'foomanconnect_fieldset', array('legend' => Mage::helper('foomanconnect')->__('Fooman Connect'))
            );
            $fieldset->addField(
                'xero_rate', 'select',
                array(
                    'name'     => "xero_rate",
                    'label'    => Mage::helper('foomanconnect')->__('Xero Rate'),
                    'title'    => Mage::helper('foomanconnect')->__('Xero Rate'),
                    'value'    => $rateModel->getXeroRate(),
                    'values'   => Mage::getModel('foomanconnect/system_taxOptions')->toOptionArray(),
                    'required' => true,
                    'class'    => 'required-entry'
                )
            );
        }

        return $this;
    }

}
