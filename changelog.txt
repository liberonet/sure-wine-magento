
v2.0.2

fix : frontend RMA requets for guest user
___________________________________________________________________________________________________________
v2.0.1

fix : permission rights/rules for non-admin magento users
___________________________________________________________________________________________________________
v2.0

- add products list in new RMA grid
- move code pool to community
- add rma manager
- add auto generate invoice
- add rma for guest account
- add acl to any actions
- add can lock rma
- fix product link in rma back
- fix rp_serials save
- add check if product is already in rma
- add return label pdf
- fix date format in rma history
- add product image in front and back view
- Add request type in product rma
- Add adjustement refund & fee in actions tab
- Do not display "Product return" tab in sales order view if user does not have the authorization
- Remove button in sales order view
- Supports online refunds
- Add product type 'simple' in sales order item
- add npai status
- add productreturn/general/is_installed key in core_config_data
- add reports in reports > product return to get stats on reason / products by periods
- prevent refund product if not invoiced
- prevent return or exchange product if not shipped
- ability to make a partial refund on shipping fees
- add email template for all product return emails and add products detail in email
- delete email templates in installation scripts
- add predefined msg in rma to fill comments

___________________________________________________________________________________________________________
v1.9.3

- Fix issue with Magento version 1.5
- Fix issue with product link edit in Product_return admin info view
___________________________________________________________________________________________________________
v1.9.2

- Add public comments on RMA view in customer account

____________________________________________________________________________________________________________
v 1.9.1

- Fix issue with magento enterprise 1.10
- Fix issue in substitution products grid (reference to the future erp release)

____________________________________________________________________________________________________________
v 1.9

- Improvement : Add configurable products value in RMA view in backend
- Improvement : Add configurable products value & manufacturer in substitution popup
- Fix : when creating exchange order, it was the configurable product that was put back to stock

____________________________________________________________________________________________________________
v 1.8.9

- Add german translation (thanks to marionwatcher !)
- new option to select the same addresses than the source order in the RMA sheet
- fix issue with state
- fix: put the right product back to stock on exchange

____________________________________________________________________________________________________________
v 1.8.8

Fix : issue in substitution product selection grid when product's name contains a '
Fix : issue to refund shipping fees under magento 1.3.x
Fix : issue in order selection grid when filtering on shipping_name column
Fix : issue on "back to stock" option for configurable products
Limit shipping method & paiement methods in combobox to 50 characters
Fix : issue with shipping & technical costs when prices include taxes
Fix : link to product in Product return
____________________________________________________________________________________________________________
v 1.8.7

- Add missing translations
- Add stock column in product reservation grid
- add new event productreturn_reservationgrid_preparecolumns
- reserved products released when RMA status set to complete
- New option in system > configuration > product return to define default status for product return requests

____________________________________________________________________________________________________________
v 1.8.6

- Add confirmation on delete button in RMA sheet
- new feature to send an email from Product return sheet using mail button (open default mail software)
- Add productreturn_order_created_for_rma event
- Add new status "Waiting for supplier" (and new email template to configure in system > configuration > product return)
- Add RMA Tab in order preparation screen (requires embedded ERP). Can be enabled in system > configuration > product return > general
- Fix : wrong product url in rma sheet
- New feature : ability to reserve products and put them in a specific warehouse. When creating an order, it reserves theses products for new order


____________________________________________________________________________________________________________
v 1.8.3

- Merge branches : same version for both Magento <1.4.1 and magento >= 1.4.1 (tests added in code to check if eav model or flat model for orders)
- Fix : add comment to generated order / credit memo to inform that it comes from a RMA
- Save search / sort parameters in session for product return grid
____________________________________________________________________________________________________________
v 1.8.2

- New option in system > configuration > product return > Product return > New status for product received to select status to affect to rma after clicking on the "products received" button
- In system > configuration > product return, add option to display or hide disabled shipping / payment methods
- Fix issue when creating order from RMA: shipping method code was wrong

____________________________________________________________________________________________________________
v 1.8.1

- Fix issue when SSL is enabled on customer account
- Displays comments (set in system > configuration > product return > pdf > comments) on the pdf
- Improve multi line text printing in PDF
- Add reason in PDF
- Fix issue for PDF header / footer to consider store scope customization
- Add new event to insert tabs in product return edit sheet
- Add new event to insert column in product return grid
- Add rp_object_type and rp_object_id in rma_products table (and fill this fields when creating an order / creditmemo)
- Improve rma shipment PDF

____________________________________________________________________________________________________________
v 1.8

- Fix issue in product return creation when order only contains virtual products
- Fix crash when no shipping method selected
- Fix issue with Magento 1.3.* (product return sheet display)
- Fix issue with bundle products

____________________________________________________________________________________________________________
v 1.7.7

- Js test to prevent from creating a product return without product in admin panel
- Add pending products menu (list products to process), pdf print
- Compliant with embedded ERP to create stock movement when product must go back to stock
- Add adminhtml.xml for Magento > 1.4
- Add a new field in product return product to store products serials
- Improved design for action tab in product return sheet
- New button "products delivered" in produc return sheet to store reception date, change product return status and notify customer from one click
- Fix: cron didn't set product return to expired
- Use JS translation for alerts

____________________________________________________________________________________________________________
v 1.7.1

- New feature to set own product reason reasons (can be set in system > config > product return)
- Column "sales order" in product return list in BO removed (also in order sheet)
- Javascript code externalised in a js/mdn/productreturn.js
_____________________________________________________________________________________________________________
v 1.7

- Rma history management
- fix : product return reason wasn't considered when customer send a product return request

