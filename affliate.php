	<?php
	
	require_once 'app/Mage.php';
	require_once 'app/code/local/Magestore/Affiliateplus/Model/Observer.php';
	
	Mage::app();
	$orders    = Mage::getModel('sales/order')
						->getCollection()
						->addFieldToFilter('entity_id','2715')
						->load();
	foreach ($orders as $order) {
		var_dump($order->getId());
		$observer['order'] = $order;
		$observer_class = new Magestore_Affiliateplus_Model_Observer();
		$observer_class->orderPlaceAfter($observer);
		$order->save();
	}
