<?php

require_once 'app/Mage.php';

Mage::app();
$dir = new DirectoryIterator(dirname(__FILE__)."\\files\\");
// $unknowns = file("NO_ITEM.txt");

$description["Ch.Ducru Beaucaillou.2nd Cru.2000 (June 2007 to May 2008)"]=290;
$description["Ch.Ducru Beaucaillou.2nd Cru.2000 ( May to Apr 2008 )"]=290;
$description["Ch.Ducru Beaucaillou.2nd Cru.2000 (June 2007 to May 2008)"]=290;
$description["Ch.Ducru Beaucaillou.2nd Cru.2000 (May 2007 to Apr 2008)"]=290;
$description["Ch.Ducru Beaucaillou.2nd Cru.2000 (May 2007 to April 2008)"]=290;
$description["Ch.Lafite Rothschild. Pauilllac.Medoc.1st Cru.2002 (July 2007 to July 2008)"]=303;
$description["Ch.Lafite Rothschild.Pauillac.Medoc.1st Cru. 1996 ( Apr to Mar 2008)"]=298;
$description["Ch.Lafite Rothschild.Pauillac.Medoc.1st Cru.1996 (May 2007 to Apr 2008)"]=298;
$description["Ch.Lafite Rothschild.Pauillac.Medoc.1st Cru.2001 (Aug 2007 to July 2008)"]=302;
$description["Ch.Latour.Pauillac.1st Growth.1998 ( Apr to Mar 2008)"]=310;
$description["Ch.Leoville Barton.2nd Cru.2000 (June 2007 to May 2008)"]=317;
$description["Ch.Leoville Barton.2nd Cru.2000 (May 2007 to Apr 2008)"]=317;
$description["Ch.Margaux.Medoc.1st Cru.1996 (Apr to Mar 2008)"]= 323;
$description["Ch.Margaux.Medoc.1st Cru.1999 (Apr to Mar 2008 )"]=325;
$description["Ch.Mouton Rothschild.Pauillac.1st Cru.2002 ( Apr to Mar 2008 )"]= 349;
$description["Ch.Mouton Rothschild.Pauillac.1st Cru.2002 ( Apr to Mar 2008)"]= 349;
$description["Ch.Pichon Longueville Baron.2nd Cru.2002 (July 2007 to June 2008)"]= 330;
$itemcodes = array("Mgmt Fee Wine 0.5% &lt;1Mil"=>367,"Mgmt Fee Wine 0.25% &gt;1Mil"=>368);
$names = array();
foreach ($dir as $fileinfo) {
   if($fileinfo->isDir()){
	   $subdir = new DirectoryIterator($fileinfo->getRealPath());
	   foreach($subdir as $file){
   if ($file->isFile()&&strstr($file->getFileName(),".xml")) {
		$doc = new DOMDocument();
		$doc->load( $file->getPath()."\\".$file->getFileName() );
		// var_dump($file->getPath()."\\".$file->getFileName() );
	$name;
	$rows = $doc->getElementsByTagName( "ROW" );//var_dump($file->getPath()."\\".$file->getFileName());
	foreach( $rows as $row )
		{    
			$name = $row->getAttribute( "COMPANYNAME" );
			if(strpos($name,"@"))
				$name = substr($name,0,strpos($name,"@"));
			$quote = Mage::getModel('sales/quote')
				->setStoreId(Mage::app()->getStore('default')->getId());
			$customer = Mage::getModel('customer/customer');
			if(isset($names[$name]))
				$customer->load($names[$name]);
			else{
				if(!$customer->setWebsiteId(1)->loadByName($name)){
					printf("UNKNOWN CUSTOMER %2\$s :%1\$s \n",$file->getPath()."\\".$file->getFileName(),$name);continue;}
					else{
						$names[$name] = $customer->getId();
					}
				}
			$quote->assignCustomer($customer);
			$primary_address = $customer->getPrimaryBillingAddress();
			if($primary_address){
				$country_id = $primary_address->getCountryId();
				$postcode = $primary_address->getPostCode();
				$region_id = $primary_address->getRegionId();
				$street = $primary_address->getStreet();
				$city = $primary_address->getCity();
				$telephone = $primary_address->getTelephone();
			}else{
				$country_id = substr($row->getAttribute( "DOCNO" ),0,2);
				$postcode = "0";
				$region_id = "0";
				$street = $row->getAttribute( "ADDRESS1" ).$row->getAttribute( "ADDRESS2" );
				$city = $row->getAttribute( "ADDRESS3" ).$row->getAttribute( "ADDRESS4" );
				$telephone = $row->getAttribute( "PHONE1" );
			}
			$products = $row->getElementsByTagName( "ROWsdsDocDetail" );
			foreach($products as $product_data){
				// $itemcode[]=
				// if($product_data->getAttribute("ITEMCODE"))
				//     printf($fn."ITEMCODE:%s\n",$product_data->getAttribute("ITEMCODE"));
				// else if($product_data->getAttribute("DESCRIPTION"))
				//     printf("DESCRIPTION:%s\n",$product_data->getAttribute("DESCRIPTION"));
				// else
				//     printf("NO ITEM:%s\n",$fn);}
				$product_id = $product_data->getAttribute("ITEMCODE");
				$product = Mage::getModel('catalog/product');
				if($product_id && $itemcodes[$product_id]) $product->load($itemcodes[$product_id]);
				else{
					// if($product_id !== "STORAGE FEE GBP LCB"){printf("%s:NO ITEM\n",$fn);continue;}
					if($product_id !== "STORAGE FEE GBP LCB" && $product_id !== "STORAGE FEE EURO PIB" && $product_id !== "STORAGE FEE USD TWC"){
						if($product_data->getAttribute("UOM")=="Half Case"||
							$product_data->getAttribute("UOM")=="HALF CASE"||
							$product_data->getAttribute("UOM")=="CASE"||
							$product_data->getAttribute("UOM")=="Case"||
							$product_data->getAttribute("UOM")=="BTL 75cl"){
							if(intval($product_data->getAttribute("RATE"))==12)
								$product_id.=" Full Case";
							else if(intval($product_data->getAttribute("RATE"))==6)
								$product_id.=" Half Case";
							else if(intval($product_data->getAttribute("RATE"))==1)
								$product_id.=" BTL 75CL";
						}
					}
							$product->load($product->getIdBySku($product_id));
					if(!$product_id && isset($description[trim($product_data->getAttribute("DESCRIPTION"))])){
						$product->load($description[trim($product_data->getAttribute("DESCRIPTION"))]);
					}else if($product_id && $product->getId()){
						$itemcodes[$product_id] = $product->getId();
					}
				}
				$buyInfo = array(
					'qty' => intval($product_data->getAttribute("QTY")),
				);
				if($product->getId()){
					$quote->addProduct($product, new Varien_Object($buyInfo));

					if(doubleval($product_data->getAttribute("UNITPRICE"))!= 0){
						$price = round(doubleval($product_data->getAttribute("UNITPRICE")),2);
						$item = $quote->getItemByProduct($product);
						$item->setCustomPrice($price);
		             	$item->setOriginalCustomPrice($price);
		             	$item->getProduct()->setIsSuperMode(true);
		             }else{
		             	if(doubleval($product_data->getAttribute("QTY"))==1) {
		             		$price = round(doubleval($product_data->getAttribute("AMOUNT")),2);
							$item = $quote->getItemByProduct($product);
							$item->setCustomPrice($price);
			             	$item->setOriginalCustomPrice($price);
			             	$item->getProduct()->setIsSuperMode(true);
		             	}
		             }
				}
				else{
					if($product_data->getAttribute("ITEMCODE"))
					printf("UNKOWN ITEM, ITEMCODE:%s\n",$product_data->getAttribute("ITEMCODE"));
				else if($product_data->getAttribute("DESCRIPTION"))
					printf("UNKOWN ITEM, DESCRIPTION:%s\n",$product_data->getAttribute("DESCRIPTION"));
				}
			}
			if(!$quote->hasItems()){printf("NO ITEM: %s\n",$file->getPath()."\\".$file->getFileName());continue;}
			$addressData = array(
				'firstname' => explode(" ",$name)[0],
				'lastname' => (explode(" ",$name)[1])?explode(" ",$name)[1]:$customer->getLastname(),
				'street' => $street,
				'city' => $city,
				'postcode' => $postcode,
				'telephone' => $telephone,
				'country_id' => $country_id,
				'region_id' =>$region_id , // id from directory_country_region table
			);
			$billingAddress = $quote->getBillingAddress()->addData($addressData);
			$shippingAddress = $quote->getShippingAddress()->addData($addressData);

			$shippingAddress->setCollectShippingRates(true)->collectShippingRates()
				->setShippingMethod('flatrate_flatrate')
				->setPaymentMethod('checkmo');
			$quote->getPayment()->importData(array('method' => 'checkmo'));

			$quote->collectTotals()->save();

			$service = Mage::getModel('sales/service_quote', $quote);
			$service->submitAll();
			$order = $service->getOrder();
			}
		}
	}
}
	}

//            printf("Created order %s\n", $order->getIncrementId());return;
//
//foreach ($dir as $fileinfo) {
//    if($fileinfo->isDir()){
//        $subdir = new DirectoryIterator($fileinfo->getRealPath());
//        foreach($subdir as $file){
//    if ($file->isFile()&&strstr($file->getFileName(),".xml")) {
//        $doc = new DOMDocument();
//        $doc->load( $file->getPath()."\\".$file->getFileName() );
////        var_dump($file->getPath()."\\".$file->getFileName() );
//
//        $name;
//        $rows = $doc->getElementsByTagName( "ROW" );//var_dump($file->getPath()."\\".$file->getFileName());
//        foreach( $rows as $row )
//        {
//            $name = substr($row->getAttribute( "COMPANYNAME" ),0,strpos($row->getAttribute( "COMPANYNAME" ),"@"));
//            $quote = Mage::getModel('sales/quote')
//            ->setStoreId(Mage::app()->getStore('default')->getId());
//
//            $customer = Mage::getModel('customer/customer')
//                ->setWebsiteId(1)
//                ->loadByName($name);
//            if(!$customer){printf("%1\$s: UNKNOWN CUSTOMER %2\$s\n",$file->getFileName(),$name);continue;}
//            $primary_address = $customer->getPrimaryBillingAddress();
//            $quote->assignCustomer($customer);
//            if($primary_address){
//                $country_id = $primary_address->getCountryId();
//                $postcode = $primary_address->getPostCode();
//                $region_id = $primary_address->getRegionId();
//                $street = $primary_address->getStreet();
//                $city = $primary_address->getCity();
//                $telephone = $primary_address->getTelephone();
//            }else{
//                $country_id = substr($row->getAttribute( "DOCNO" ),0,2);
//                $postcode = "0";
//                $region_id = "0";
//                $street = $row->getAttribute( "ADDRESS1" ).$row->getAttribute( "ADDRESS2" );
//                $city = $row->getAttribute( "ADDRESS3" ).$row->getAttribute( "ADDRESS4" );
//                $telephone = $row->getAttribute( "PHONE1" );
//            }
//            $products = $row->getElementsByTagName( "ROWsdsDocDetail" );
//            foreach($products as $product_data){
//                $part = false;
//                $product_id = $product_data->getAttribute("ITEMCODE");
//                if($product_data->getAttribute("UOM")=="CASE"||$product_data->getAttribute("UOM")=="Case"){
//                    if(intval($product_data->getAttribute("RATE"))==12)
//                        $product_id.=" Full Case";
//                    else if(intval($product_data->getAttribute("RATE"))==6)
//                        $product_id.=" Half Case";
//                    else
//                        $part = true;
//                }
//                if(!$product_id) continue;
//                $product = Mage::getModel('catalog/product');
//                if($part)
//                    $product->load($product->getIdByPartSku($product_id));
//                else
//                    $product->load($product->getIdBySku($product_id));
//                if(!$product->getId()) continue;
//                $buyInfo = array(
//            'qty' => intval($product_data->getAttribute("QTY")),
//            );
//            $quote->addProduct($product, new Varien_Object($buyInfo));
//            }
//            if(!$quote->hasItems()){printf("%s:NO ITEM\n",$file->getFileName());continue;}
//            $addressData = array(
//                'firstname' => explode(" ",$name)[0],
//                'lastname' => (explode(" ",$name)[1])?explode(" ",$name)[1]:$customer->getLastname(),
//                'street' => $street,
//                'city' => $city,
//                'postcode' => $postcode,
//                'telephone' => $telephone,
//                'country_id' => $country_id,
//                'region_id' =>$region_id , // id from directory_country_region table
//            );
//            $billingAddress = $quote->getBillingAddress()->addData($addressData);
//            $shippingAddress = $quote->getShippingAddress()->addData($addressData);
//
//            $shippingAddress->setCollectShippingRates(true)->collectShippingRates()
//                ->setShippingMethod('flatrate_flatrate')
//                ->setPaymentMethod('checkmo');
//            $quote->getPayment()->importData(array('method' => 'checkmo'));
//
//            $quote->collectTotals()->save();
//
//            $service = Mage::getModel('sales/service_quote', $quote);
//            $service->submitAll();
//            $order = $service->getOrder();
////            printf("Created order %s\n", $order->getIncrementId());return;
//                }
//            }
//        }
//    }
//}